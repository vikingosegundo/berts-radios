//
//  Bert_s_RadiosApp.swift
//  Bert's Radios
//
//  Created by vikingosegundo on 15/12/2021.
//

import SwiftUI
import BertUI
import BertModel
import BertsApp

@main
final
class BertsRadiosApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(viewState:viewState)
        }
    }
    private      let store      : Store           = createDiskStore(pathInDocs:"state.json")
    private lazy var viewState  : ViewState       = ViewState(store:store, roothandler:{ self.rootHandler($0) })
    private lazy var rootHandler: (Message) -> () = createAppDomain(
        store      : store,
        receivers  : [ viewState.handle(msg:), expandLoggingMessages({ self.rootHandler($0) }) ],
        rootHandler: { self.rootHandler($0) }
    )
}

fileprivate
func expandLoggingMessages(_ rootHandler: @escaping (Message) -> ()) -> (Message) -> () {
    {
        if case let .radio(.add   (s,  to:.favorites)) = $0 { rootHandler(.logging(.write(LogItem(text:"station «\(s.name)» was added to favorites"    )))) }
        if case let .radio(.remove(s,from:.favorites)) = $0 { rootHandler(.logging(.write(LogItem(text:"station «\(s.name)» was removed from favorites")))) }
    }
}
