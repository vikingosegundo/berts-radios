//
//  createAppDomain.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 11.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Foundation
import AudioStreaming
import BertModel

public typealias  Input = (Message) -> ()
public typealias Output = (Message) -> ()

public func createAppDomain(
    store      : AppStore,
    receivers  : [Input],
    rootHandler: @escaping Output) -> Input
{
    let features: [Input] = [
        createRadioFeature(store:store, player:AudioPlayer(), output:rootHandler),
        createLoggingFeature(logger:Logger())
    ]
    return { msg in
        (receivers + features).forEach { $0(msg) }
    }
}
