//
//  Visualizer.swift
//  Bert's Radios
//
//  Created by vikingosegundo on 22/12/2021.
//

import AVKit
import AudioStreaming
import Accelerate
import BertModel

func request(_ visualiser:Visualiser, to req:Visualiser.Request) { visualiser.request(to: req) }
struct Visualiser:UseCase {
    enum Request {
        case   publishLoudness
        case unPublishLoudness }
    enum Response {
        case loudness(interpolated:[Float])
    }
    init(audioplayer p: AudioPlayer,
         store       s: AppStore,
         responder   r: @escaping (Response) -> ()
    ) {
        interactor = Interactor(player:p, store:s, responder:r)
    }
    func request(to request: Request) {
        switch request {
        case   .publishLoudness:interactor  .publish()
        case .unPublishLoudness:interactor.unpublish()
        }
    }
    private let interactor: Interactor
    typealias  RequestType = Request
    typealias ResponseType = Response
}
// MARK: - private
private extension Visualiser {
    private final class Context {
        init(player p: AudioPlayer) { player = p }
        enum Change {
            case prevRMSValue       (Float       )
            case interpolatedResults([Float]     )
            case vizprocessing      (FilterEntry?)
            case publishLoudness    (Bool        )
        }
        func change(_ c:Change) {
            switch c {
            case .prevRMSValue       (let v): prevRMSValue        = v
            case .interpolatedResults(let v): interpolatedResults = v
            case .vizprocessing      (let v): vizprocessing       = v
            case .publishLoudness    (let v): publishLoudness     = v
            }
        }
        private      let player             : AudioPlayer
        private(set) var prevRMSValue       : Float   = 0.3
        private(set) var interpolatedResults: [Float] = []
        private(set) var vizprocessing      : FilterEntry?
        private(set) var publishLoudness    = true {
            didSet {
                guard let processAudioData = vizprocessing else { return }
                publishLoudness
                    ? player.frameFiltering.add(entry: processAudioData)
                    : player.frameFiltering.removeAll()
            }
        }
    }
    struct Interactor {
        init(player    p: AudioPlayer,
             store     s: AppStore,
             responder r: @escaping (Response) -> ()
        ) {
            player  = p
            respond = r
            store   = s
            context = Context(player: p)
        }
        private let context: Context
        private let player : AudioPlayer
        private let respond: (Response) -> ()
        private let store  : AppStore
    }
}
private extension Visualiser.Interactor {
    func publish() {
        if context.vizprocessing == nil {
            context.change(.vizprocessing(
                FilterEntry(name:"vizprocessing") { buffer, when in
                    self.processAudioData(buffer:buffer)
                    self.respond(.loudness(interpolated:self.context.interpolatedResults))
                }
            ))
        }
        context.change(.publishLoudness(store.state().visualisationActive))
    }
    func unpublish() {
        context.change(.publishLoudness(false))
    }
}
private extension Visualiser.Interactor{
    private func processAudioData(buffer: AVAudioPCMBuffer){
        guard
            let channelData = buffer.floatChannelData?[0]
        else { return }
        let frames = buffer.frameLength
        let rmsValue = SignalProcessing.rms(data:channelData, frameLength:UInt(frames))
        context.change(.interpolatedResults(SignalProcessing.interpolate(current:rmsValue, previous:context.prevRMSValue)))
        context.change(.prevRMSValue(rmsValue))
    }
}
private enum SignalProcessing {
    static func db(from val:Float) -> Float { 160 + 10 * log10f(val) - 120 }
    static func rms(data: UnsafeMutablePointer<Float>, frameLength: UInt) -> Float {
        var val: Float = 0
        vDSP_measqv(data, 1, &val, frameLength)
        return min(max(0.3 + db(from:val)/Float(40/0.3), 0.3), 0.6)
    }
    static func interpolate(current: Float, previous: Float) -> [Float]{
        var vals:[Float] = .init(repeating: 0, count: 11)
        vals[10] = current
        vals[ 5] = (current  + previous)/2
        vals[ 2] = (vals[ 5] + previous)/2
        vals[ 1] = (vals[ 2] + previous)/2
        vals[ 8] = (vals[ 5] + current )/2
        vals[ 9] = (vals[10] + current )/2
        vals[ 7] = (vals[ 5] + vals[ 9])/2
        vals[ 6] = (vals[ 5] + vals[ 7])/2
        vals[ 3] = (vals[ 1] + vals[ 5])/2
        vals[ 4] = (vals[ 3] + vals[ 5])/2
        vals[ 0] = (previous + vals[ 1])/2
        return vals + vals.dropFirst()
    }
}
