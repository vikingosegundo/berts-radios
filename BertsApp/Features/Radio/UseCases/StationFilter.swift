//
//  StationFilter.swift
//  Bert's Radios
//
//  Created by vikingosegundo on 23/12/2021.
//

import BertModel

func request(_ filter:StationFilter, to req:StationFilter.Request) { filter.request(to:req) }
public struct StationFilter: UseCase {
    public enum Request {
        case filter(by:_By); public enum _By {
            case name(String) }
        case clear }
    public enum Response{
        case filtered(by:_By); public enum _By {
            case name(String, stations:[Station]) }
        case filteredCleared
    }
    public init(
        store s: AppStore,
        responder r: @escaping (Response) -> ()
    ) {
        store = s
        respond = r
    }
    public func request(to request:Request) {
        switch request {
        case let .filter(by:.name(name)):
            let filteredStations = store.state().stations.filter { $0.name.lowercased().contains(name.lowercased()) }
            store.change(
                .replace(
                    .filtered(with:filteredStations) ) )
            respond(.filtered(by:.name(name,stations:filteredStations)))
        case .clear: store.change(.replace(.filtered(with: [])))
            respond(.filteredCleared)
        }
    }
    private let respond: (Response) -> ()
    private let store  : AppStore
    typealias  RequestType = Request
    typealias ResponseType = Response
}
