//
//  StationLoader.swift
//  Bert's Radios
//
//  Created by vikingosegundo on 16/12/2021.
//

import BertModel

func request(_ loader:StationLoader, to req:StationLoader.Request) { loader.request(to: req) }
struct StationLoader: UseCase {
    enum Request {
        case load(_All); enum _All {
            case all(_Stations); enum _Stations {
                case stations } } }
    enum Response {
        case all(_Stations); enum _Stations {
            case stations([Station], loaded:Result) } }
    init(store:AppStore, responder: @escaping (Response) -> ()) {
        self.store   = store
        self.respond = responder
    }
    func request(to request:Request) {
        let stations = [
            Station(name:"Sublime" ).alter(.add(.url("https://playerservices.streamtheworld.com/api/livestream-redirect/SUBLIME.mp3" ))),
            Station(name:"538"     ).alter(.add(.url("https://playerservices.streamtheworld.com/api/livestream-redirect/RADIO538.mp3"))),
            Station(name:"Veronica").alter(.add(.url("https://playerservices.streamtheworld.com/api/livestream-redirect/VERONICA.mp3"))),
            Station(name:"Cosmo"   ).alter(.add(.url("https://wdr-cosmo-live.icecastssl.wdr.de/wdr/cosmo/live/mp3/128/stream.mp3"    )))
        ]
        switch request {
        case .load(.all(.stations)):
            stations.forEach {
                store.state().stations.map{$0.name}.contains($0.name)
                ? ()
                : store.change(.add($0, to:.stations))
            }
            respond(.all(.stations(stations, loaded:.successfully)))
        }
    }
    
    private let store  : AppStore
    private let respond: (Response) -> ()
    typealias  RequestType = Request
    typealias ResponseType = Response
}
