//
//  VisualisationActivater.swift
//  Bert's Radios
//
//  Created by vikingosegundo on 04/02/2022.
//

import BertModel

func request(_ activator:VisualisationActivator, to req:VisualisationActivator.Request) { activator.request(to: req) }
struct VisualisationActivator: UseCase {
    enum Request {
        case activate
        case deactivate }
    enum Response {
        case   activation(_Response)
        case deactivation(_Response)
        enum _Response {
            case succeeded }
    }
    init(store     s: AppStore,
         responder r: @escaping (Response) -> ()
    ) {
        store   = s
        respond = r
    }
    func request(to request:Request) {
        switch request {
        case   .activate: store.change(  .activate(.visualisation)); respond(  .activation(.succeeded))
        case .deactivate: store.change(.deactivate(.visualisation)); respond(.deactivation(.succeeded))
        }
    }
    typealias  RequestType = Request
    typealias ResponseType = Response
    private let store  : AppStore
    private let respond: (Response) -> ()
}
