//
//  Player.swift
//  Bert's Radios
//
//  Created by vikingosegundo on 16/12/2021.
//
import AVKit
import AudioStreaming
import Accelerate
import BertModel
import BertTools

func request(_ player:Player, to req:Player.Request) { player.request(to: req) }
struct Player: UseCase {
    enum Request  {
        case play(Station)
        case pause
    }
    enum Response {
        case playing(String, on:Station)
        case paused (Station)
        case waiting(Station)
    }
    init(audioplayer p: AudioPlayer,
         store       s: AppStore,
         responder   r: @escaping (Response) -> ()
    ) {
        interactor = Interactor(audioplayer:p, store:s, respond:r)
    }
    func request(to request:Request) {
        switch request {
        case .play(let s): play (station: s)
        case .pause      : pause()
        }
    }
    private let interactor: Interactor
    typealias  RequestType = Request
    typealias ResponseType = Response
}

// MARK: - private
private extension Player {
    func play (station:Station) { interactor.play(station:station) }
    func pause()                { interactor.pause()               }
}
private extension Player {
    private struct Interactor {
        init(audioplayer p: AudioPlayer,
             store       s: AppStore,
             respond     r: @escaping (Response) -> ()
        ) {
            player  = p
            store   = s
            respond = r
            delegate = AudioPlayerDelegate(audioplayer: p, store: s, respond: r)
        }
        func play(station s:Station) {
            let session = AVAudioSession.sharedInstance()
            do {
                try session.setActive(true)
                try session.setCategory(.playback, mode:.default, options:.defaultToSpeaker)
            } catch { print(error.localizedDescription) }
            if let address = s.urls.last?.address {
                guard let url = URL(string:address) else { return }
                player.play(url:url)
                change(store, .current(.playing(songname:"",on:s)))
                respond(.playing("", on:s))
            }
        }
        func pause() {
            player.stop()
            guard
                let station = station(from:store.state().currentStation)
            else { return }
            store.change(.current(.paused(station)))
            respond(.paused(station))
        }
        private let player  : AudioPlayer
        private let store   : AppStore
        private let respond : (Response) -> ()
        private let delegate: Player.AudioPlayerDelegate
    }
}
private extension Player {
    final class AudioPlayerDelegate {
        init(audioplayer p: AudioPlayer,
             store       s: AppStore,
             respond     r: @escaping (Player.Response) -> ()
        ) {
            player  = p
            store   = s
            respond = r
            player.delegate = self
        }
        private let player  : AudioPlayer
        private let store   : AppStore
        private let respond : (Player.Response) -> ()
    }
}
extension Player.AudioPlayerDelegate : AudioPlayerDelegate {
    func audioPlayerDidReadMetadata(player: AudioPlayer, metadata m: [String : String]) {
        let key = "StreamTitle"
        let streamTitle = m.keys.contains(key) ? m[key]! : ""
        change(store, .current(.playing(songname:streamTitle,on:station(from: store.state().currentStation)!)))
        respond(.playing(streamTitle,on:station(from:store.state().currentStation)!))
    }
}
