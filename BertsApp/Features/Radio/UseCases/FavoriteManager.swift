//
//  FavoriteManager.swift
//  Bert's Radios
//
//  Created by vikingosegundo on 29/12/2021.
//

import BertModel

func request(_ manager:FavoriteManager, to req:FavoriteManager.Request) { manager.request(to:req) }
struct FavoriteManager: UseCase {
    enum Request{
        case add(_Add); enum _Add {
            case favorite(Station) }
        case remove(_Remove); enum _Remove {
            case favorite(Station) }
    }
    enum Response{
        case station(Station, _Was); enum _Was {
            case was(_Response); enum _Response {
                case added
                case removed } }
    }
    init(store     s: AppStore,
         responder r: @escaping (Response) -> ()
    ) {
        store   = s
        respond = r
    }
    func request(to r:Request) {
        switch r {
        case let .add   (.favorite(station)): store.change(.add   (station,to  :.favourites)); respond(.station(station,.was(.added  )))
        case let .remove(.favorite(station)): store.change(.remove(station,from:.favourites)); respond(.station(station,.was(.removed)))
        }
    }
    private let store  : AppStore
    private let respond: (Response) -> ()
    typealias RequestType = Request
    typealias ResponseType = Response
}
