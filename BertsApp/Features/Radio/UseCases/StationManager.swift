//
//  StationManager.swift
//  BertsApp
//
//  Created by vikingosegundo on 16/02/2022.
//

import BertModel

func request(_ stationManager:StationManager, to req:StationManager.Request) { stationManager.request(to:req) }
public struct StationManager: UseCase {
    public enum Request {
        case all(_All); public enum _All { case stations }
    }
    public enum Response {
        case all(stations: [Station])
        case filtered([Station], by:Filter)
    }
    public init(
        store     s: AppStore,
        responder r: @escaping (Response) -> ()
    ) {
        store   = s
        respond = r
    }
    public func request(to change:Request) {
        switch change {
        case .all(.stations):store.change(.replace(.settingStations(with: store.state().stations                                  ))); respond(.all(stations: store.state().stations))
        }
    }
    typealias RequestType = Request
    typealias ResponseType = Response
    private let store  : AppStore
    private let respond: (Response) -> ()
}
