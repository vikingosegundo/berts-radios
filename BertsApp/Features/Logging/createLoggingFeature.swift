//
//  createLoggingFeature.swift
//  BrighterHue
//
//  Created by vikingosegundo on 22/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import BertModel
import BertTools

public func createLoggingFeature(logger l:Logging) -> Input {
    let writer = LogitemWriter(logger: l)
    func execute(command cmd:Message.Logging) {
        switch cmd {
        case let .write(item): request(writer, to:.write(item:item))
        }
    }
    return { // entry point
        if case .logging(let c) = $0 { execute(command:c) }
    }
}
