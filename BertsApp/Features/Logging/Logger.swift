//
//  Logger.swift
//  BertsApp
//
//  Created by Manuel Meyer on 03.05.22.
//

import BertModel
import os
typealias OSLogger = os.Logger

public protocol Logging {
    func log(_ item: LogItem)
}

public struct Logger: Logging {
    public init() {}
    public func log(_ item: LogItem) {
        let logger = OSLogger()
        logger.log(level: logType(for:item), "\(icon(for:item))[\(level(of:item).padding(toLength:5))]: \(item.text)")
    }
}
fileprivate func logType(for li:LogItem) -> OSLogType {
    switch li.logLevel {
    case .info : return .debug
    case .warn : return .error
    case .error: return .error
    case .custom: return .info
    }
}
fileprivate func icon(for li: LogItem) -> String {
    switch li.logLevel {
    case .info  : return "✅"
    case .warn  : return "⚠️"
    case .error : return "🧨"
    case .custom: return "⚙️"
    }
}
fileprivate func level(of li: LogItem) -> String {
    switch li.logLevel {
    case .info         : return "info"
    case .warn         : return "warn"
    case .error        : return "error"
    case .custom(let s): return s
    }
}
