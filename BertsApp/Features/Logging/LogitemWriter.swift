//
//  LogitemWriter.swift
//  BrighterHue
//
//  Created by vikingosegundo on 24/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import BertModel

func request(_ writer:LogitemWriter, to r:LogitemWriter.Request) { writer.request(to:r) }
struct LogitemWriter:UseCase {
    enum Request  { case write(item:LogItem) }
    enum Response {                          }
    init(logger l: Logging) { logger = l }
    func request(to request:Request) {
        switch request {
        case let .write(item: item): logger.log(item)
        }
    }
    typealias RequestType = Request
    typealias ResponseType = Response
    private let logger: Logging
}
