//
//  PetriDishNoUIApp.swift
//  PetriDishNoUI
//
//  Created by vikingosegundo on 18/02/2022.
//

import SwiftUI
import BertUI
import BertModel
import BertsApp
private let formatter = ISO8601DateFormatter()


private let duration = 3 * 60
private let info = (
    test: (
        timer : (
            initial: Command.Offset.seconds(0),
            first : ( offset: Command.Offset.milliseconds(       750), duration: Command.Duration.seconds(duration)),
            second: ( offset: Command.Offset     .seconds(duration/2), duration: Command.Duration.seconds(duration))
        ),
        noop : false,
        id   : UUID(),
        start: Date.now
    ),
    biohazard: (
        image: (
            name   : "biohazard",
            color  : Color.orange,
            opacity: 0.5
        ),
        title: (
            text: "NOT A UI TEST",
            size: 48.0
        )
    ),
    missingStation:(
        name   : "OoopsFM",
        address: "https://oops.fm"
    )
)

@main
final
class PetriDishNoUIApp: App {
    init() { run(rootHandler, viewState) }
    var body: some Scene {
        WindowGroup {
            ZStack {
                EmptyView().background(.black)
                Image(info.biohazard.image.name)
                    .background(.clear)
                    .foregroundColor(info.biohazard.image.color)
                    .opacity        (info.biohazard.image.opacity)
                
                VStack {
                    Text(info.test.id.uuidString)
                    Text(formatter.string(from:info.test.start))
                    Text(info.test.start.timeIntervalSince1970.description)
                }
            }
        }
    }
    private      let store      : Store                                        = createDiskStore()
    private lazy var run        : (@escaping (Message) -> (), ViewState) -> () = createTestProgram()
  
 
    private lazy var viewState  : ViewState       = ViewState(store: store, roothandler: { let s = string(from:$0); s != "" ? print(s) : (); self.rootHandler($0) })
    private lazy var rootHandler: (Message) -> () = createAppDomain(
        store      : store,
        receivers  : [ viewState.handle(msg:) ],
        rootHandler: { self.viewState.roothandler($0) }
    )
}

let start = Date.now
func string(from msg: Message) -> String {
    switch msg {
    case .radio(.visualizer): return ""
    case let .radio(.play(           s)): return "====>(\(String(format: "% 12.5f",Date.now.timeIntervalSince1970 - start.timeIntervalSince1970))) .radio(.play(\(s.name)))"
    case let .radio(.playing(song,on:s)): return "====>(\(String(format: "% 12.5f",Date.now.timeIntervalSince1970 - start.timeIntervalSince1970))) .radio(.playing(\(song),on:\(s.name)))"
    default: return "====> \(msg)"
    }
}

private
enum Command {
    enum Duration { case seconds(Int), milliseconds(Int) }
    enum Offset   { case seconds(Int), milliseconds(Int) }
    case execute(repeat:Bool, Offset, Duration, () -> ())
}

private
func createTestProgram() -> (@escaping (Message) -> (), ViewState) -> () {
    func timeinterval(from ci:Command.Duration) -> TimeInterval { switch ci { case let .seconds(interval): return TimeInterval(interval)           ; case let .milliseconds(interval): return                  TimeInterval(Float(interval) / 1000.0)  } }
    func firedate    (from  o:Command.Offset  ) -> Date         { switch  o { case let .seconds(s       ): return .now.advanced(by:TimeInterval(s)); case let .milliseconds(interval): return .now.advanced(by:TimeInterval(Float(interval) / 1000.0)) } }
    return { input, vs in
        ([Command
            .execute(repeat: false, info.test.timer.initial,       info.test.timer.first .duration, { input(.radio(.visualizer(.activate))        ) }),
            .execute(repeat: false, info.test.timer.initial,       info.test.timer.first .duration, { input(.radio(.load(.all(.stations)))        ) }),
            .execute(repeat: true,  info.test.timer.first .offset, info.test.timer.first .duration, { input(.radio(.play(randomStation(from:vs))) ) }),
            .execute(repeat: true,  info.test.timer.second.offset, info.test.timer.second.duration, { input(.radio(.play(randomStation(from:vs))) ) }),
         ]).forEach {
            switch $0 {
            case let .execute(repeat: r, o, i, b):
                RunLoop.current.add(
                    Timer(
                        fire    : firedate(from: o),
                        interval: timeinterval(from: i),
                        repeats : r,
                        block   : { _ in b() }
                    ),
                    forMode: .common
                )
            }
        }
    }
}
private func randomStation(from vs:ViewState) -> Station {
    vs.stations.filter {
        isCurrentlyPlaying(viewState:vs)
            ? $0.id != currentStation(in:vs).id
            : true
    }.randomElement()!
}
private
func isCurrentlyPlaying(viewState: ViewState) -> Bool {
    switch viewState.currentlyPlaying {
    case .playing(songname: _, on: _): return true
    default: return false
    }
}
private
func currentStation(in vs:ViewState) -> Station {
    switch vs.currentlyPlaying {
    case let .playing(songname:_, on:s): return s
    default                            : return Station(name:info.missingStation.name).alter(.add(.url(info.missingStation.address)))
    }
}
