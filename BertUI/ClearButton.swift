//
//  ClearButton.swift
//  Bert's Radios
//
//  Created by vikingosegundo on 23/12/2021.
//

import SwiftUI

public struct ClearButton: ViewModifier {
    @Binding var text: String
    private let iconName:String
    private let block: () -> ()
    public init(text           t: Binding<String>,
                systemIconName s: String = "multiply.circle.fill",
                _              b: @escaping () -> ())
    {
        _text    = t
        iconName = s
        block    = b
    }
    public func body(content: Content) -> some View {
        HStack {
            content
            Spacer()
            Image(systemName: iconName)
                .foregroundColor(.secondary)
                .opacity(text == "" ? 0 : 1)
                .onTapGesture { self.text = ""; block() }
        }
    }
}
