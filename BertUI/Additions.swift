//
//  Additions.swift
//  Bert's Radios
//
//  Created by vikingosegundo on 05/01/2022.
//

import SwiftUI
public let strokes = (
    color: colors.stroke,
    width: 1.0
)
public extension View {
    func stroke(color: Color = strokes.color, width:Double = strokes.width) -> some View {
        modifier(Stroke(width:strokes.width, color:strokes.color))
    }
}
public struct Stroke: ViewModifier {
    let width: CGFloat
    let color: Color
    public func body(content: Content) -> some View {
        content.shadow(color: color, radius: width, x: 0, y: 0)
    }
}
