//
//  Settings.swift
//  BertUI
//
//  Created by vikingosegundo on 04/02/2022.
//

import SwiftUI
import BertModel
import BertsApp

public struct Settings: View {
    public var body: some View {
        NavigationView {
            ZStack {
                if viewState.settingsshown {
                    backgroundView()
                    foregroundView()
                }
            }
            .navigationBarTitle(labels.settings.title, displayMode: .inline)
            .listStyle(InsetGroupedListStyle())
        }
        .navigationViewStyle(StackNavigationViewStyle())
        .onAppear    { self.viewState.settingsshown = true  }
        .onDisappear { self.viewState.settingsshown = false }
    }
    @EnvironmentObject private var viewState: ViewState
}

//MARK: - private
private extension Settings {
    func backgroundView() -> some View {
        VStack {
            EmptyView().visualisation(viewState:viewState)
        }
    }
    func foregroundView() -> some View {
        VStack {
            List {
                Section(header:ZStack(alignment: .leading) {
                    colors.bg.list.header.edgesIgnoringSafeArea(.all)
                    Text(labels.settings.visualisation.title)
                        .foregroundColor(.black)
                }.listRowInsets(EdgeInsets())) {
                    Button {
                        viewState.roothandler(.radio(.visualizer(viewState.visualisationActive == true ? .deactivate : .activate)))
                    } label: {
                        Text(viewState.visualisationActive
                             ? labels.settings.visualisation.deactivate
                             : labels.settings.visualisation.activate  ).stroke()
                    }
                }.listRowBackground(colors.bg.list.row)
                Section(header:ZStack(alignment: .leading) {
                    colors.bg.list.header.edgesIgnoringSafeArea(.all)
                    Text(labels.settings.station.management)
                        .foregroundColor(.black)
                }.listRowInsets(EdgeInsets())){
                    NavigationLink(destination: StationManaging()) {
                        Text(labels.settings.station.manage).stroke()
                    }
                }.listRowBackground(colors.bg.list.row)
            }
        }
    }
}
