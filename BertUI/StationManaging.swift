//
//  StationManaging.swift
//  BertUI
//
//  Created by vikingosegundo on 16/02/2022.
//

import SwiftUI
import BertModel
import BertsApp

public struct StationManaging:View {
    public init(){
        configureNavigationBar()
    }
    public var body: some View {
        ZStack {
            backgroundView()
            foregroundView()
        }
    }

    @EnvironmentObject private var viewState  : ViewState
}
// MARK: - private
private extension StationManaging {
    func backgroundView() -> some View {
        VStack {
            EmptyView().visualisation(viewState: viewState)
        }
    }
    func foregroundView() -> some View {
        VStack {
            List {
                ForEach(viewState.settingsStations) {
                    listItemFor($0)
                }.listRowBackground(colors.bg.list.row)
            }
            .navigationBarTitle(labels.settings.station.manage, displayMode: .inline)
        }.onAppear { viewState.roothandler(.settings(.fetch(.all(.stations)))) }
        .ignoresSafeArea(edges: .all)
        .toolbar {
            Button {
            } label: {
                Image(systemName:icons.add)
            }
        }
    }
    func configureNavigationBar() {
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        appearance.backgroundColor = UIColor(colors.bg.navigation)
        let proxy = UINavigationBar.appearance()
        proxy.tintColor = .white
        proxy.standardAppearance = appearance
        proxy.scrollEdgeAppearance = appearance
    }
    func listItemFor(_ station:Station) -> some View {
        HStack {
            VStack {
                Text("\(station.name)")
            }
            Spacer()
        }.stroke()
    }
}
