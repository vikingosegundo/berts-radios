//
//  ViewState.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 11.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import SwiftUI
import BertModel

public final class ViewState: ObservableObject {
    @Published public var stations          : [Station] = []
    @Published public var filteredStations  : [Station] = []
    @Published public var favoriteStations  : [Station] = []
    @Published public var settingsStations  : [Station] = []
    @Published public var unfavoriteStations: [Station] = []

    @Published public var currentlyPlaying    : Current = .nonePlaying
    @Published public var loudness            : [Float] = []
    @Published public var cursor              = 0
    @Published public var visualisationActive = true
    @Published public var roothandler         : (Message) -> ()
    @Published public var settingsshown       = false

    public init(store:AppStore, roothandler: @escaping (Message) -> ()) {
        self.roothandler = roothandler
        store.updated { self.process(state(in:store)) }
        process(state(in: store))
    }
    public func handle(msg: Message) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if case let .radio(.visualizer(.loudness(interpolated:values))) = msg { self.cursor           = (self.cursor + 1) % max(self.loudness.count, 1); self.loudness = values.enumerated().map { k,v in Float(k + 1) * v } }
            if case let .radio(.playing   (name, on:station)              ) = msg { self.currentlyPlaying = .playing(songname:name,on:station) }
            if case let .radio(.paused    (         station)              ) = msg { self.currentlyPlaying = .paused (                 station) }
        }
    }
    public func process(_ appState: AppState) {
        DispatchQueue.main.async {
            self.stations            = appState.stations.sort()
            self.filteredStations    = appState.filteredStations.sort()
            self.favoriteStations    = appState.favoriteStations.sort()
            self.settingsStations    = appState.settingsStations.sort()
            self.unfavoriteStations  = Array(Set(appState.stations).subtracting(Set(appState.favoriteStations))).sort()
            self.visualisationActive = appState.visualisationActive
        }
    }
}

fileprivate
extension Array where Element == Station {
    func sort() -> Self {
        sorted {
            $0.name.localizedCaseInsensitiveCompare($1.name) == .orderedAscending
        }
    }
}
