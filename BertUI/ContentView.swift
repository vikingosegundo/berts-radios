//
//  ContentView.swift
//  Bert's Radios
//
//  Created by vikingosegundo on 15/12/2021.
//

import SwiftUI
import BertModel
    
public struct ContentView: View {
    public init(viewState: ViewState) {
        self.viewState   = viewState
        self.roothandler = viewState.roothandler
        UITabBar.appearance().backgroundColor = UIColor(colors.bg.navigation)
    }
    public var body: some View {
        TabView {
            Stations().tabItem { Label(labels.tabBar.player,   systemImage:icons.tabBar.player  ) }
            Settings().tabItem { Label(labels.tabBar.settings, systemImage:icons.tabBar.settings) }
        }
        .onAppear { roothandler(Message.radio(.load(.all(.stations)))) }
        .environmentObject(viewState)
        .accentColor(colors.tabbar.accent)
        .tint       (colors.tabbar.tint  )
    }
    @ObservedObject
    private var viewState  : ViewState
    var roothandler: (Message) -> ()
}
