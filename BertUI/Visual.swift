//
//  Visual.swift
//  BertUI
//
//  Created by vikingosegundo on 16/02/2022.
//

import SwiftUI

struct Visual: ViewModifier {
    var loudness: [Float]
    var cursor  : Int
    func body(content: Content) -> some View {
        GeometryReader { geometry in
            VStack {
                LazyVGrid(columns: Array(repeating: GridItem(GridItem.Size.fixed(geometry.size.width / 3), spacing:0), count:3), spacing:0) {
                    ForEach(rotateLoudnessValuesByCursor(), id:\.0) { i, l in
                        Rectangle()
                            .frame(width:geometry.size.width / 3, height: geometry.size.height / 7)
                            .foregroundColor( colors.viz(l) )
                    }
                }
            }
        }.edgesIgnoringSafeArea(.all)
    }
    
    func rotateLoudnessValuesByCursor() -> [(Int, Double)] {
        Array(zip(loudness.indices.suffix(loudness.indices.count - cursor)
                + loudness.indices.prefix(cursor + 1                     )
                  , loudness.map{Double($0)}))
    }
}
extension View {
    func visualisation(viewState:ViewState) -> some View {
        modifier(Visual(loudness: viewState.loudness, cursor: viewState.cursor))
    }
}
