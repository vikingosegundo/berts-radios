//
//  Labels.swift
//  BertUI
//
//  Created by vikingosegundo on 16/02/2022.
//

import SwiftUI

public let labels = (
    tabBar: (
        player  :"Player",
        settings:"Settings"
    ),
    play          :"play",
    pause         :"pause",
    searchStations:"Search Stations",
    settings: (
        title:"Settings",
        visualisation: (
                 title:"Visualisation",
              activate:  "activate visualisation",
            deactivate:"deactivate visualisation"
        ),
        station: (
            manage    :"Manage Stations",
            management:"Station Management",
            remove    :"remove Station",
            add       :"add station"
        )
    )
)
