//
//  Colors.swift
//  BertUI
//
//  Created by vikingosegundo on 16/02/2022.
//
import SwiftUI

public let colors = (
    bg    :(
        list: (
            row   :Color.black.opacity(0.75),
            header:Color(white:1, opacity:0.15)
        ),
        button: (
            play :Color.black.opacity(0.5),
            pause:Color.black.opacity(0.5)
        ),
        navigation:Color.black.opacity(0.75)
    ),
    viz   :{ Color(hue:$0 / 11.0, saturation:$0 / 18.0, brightness:1.0) },
    tabbar:(
        accent:Color.white,
        tint  :Color.white
    ),
    stroke:Color.black
)
