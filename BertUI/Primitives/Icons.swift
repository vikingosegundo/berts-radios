//
//  Icons.swift
//  BertUI
//
//  Created by vikingosegundo on 16/02/2022.
//

import SwiftUI

public let icons = (
    pause    : (  filled:"pause.fill",    unfilled:"pause"),
    favourite: (selected: "star.fill", notSelected:"star" ),
    playing: (
         artist:"guitars",
           song:"music.quarternote.3",
        station:"radio"
    ),
    add   :"plus",
    tabBar: (
        player  :"radio",
        settings:"gear"
    )
)
