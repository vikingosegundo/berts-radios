//
//  Stations.swift
//  Bert's Radios
//
//  Created by vikingosegundo on 23/12/2021.
//

import SwiftUI
import BertModel

public struct Stations: View {
    public var body: some View {
        NavigationView {
            ZStack {
                backgroundView()
                foregroundView()
            }
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarHidden(true)
        }
        .onReceive(NotificationCenter.default.publisher(for:UIApplication.willEnterForegroundNotification)) { _ in viewState.roothandler(.radio(.visualizer(.start))) }
        .onReceive(NotificationCenter.default.publisher(for:UIApplication .didEnterBackgroundNotification)) { _ in viewState.roothandler(.radio(.visualizer(.stop ))) }
    }
    @State             private var searchText     : String = ""
    @FocusState        private var searchIsFocused: Bool { didSet { viewState.roothandler(.radio(.clearFilter)) } }
    @EnvironmentObject private var viewState      : ViewState
}

//MARK: - private
private extension Stations {
    var isInSearchMode:Bool {
        searchText.trimmingCharacters(in:.whitespacesAndNewlines) != ""
    }
    func foregroundView() -> some View {
        VStack {
            stations()
            info()
            Spacer()
            if !isInSearchMode {
                miniplayer()
            }
        }
    }
    func backgroundView() -> some View {
        VStack {
            EmptyView().visualisation(viewState:viewState)
        }
    }
    func listItemFor(_ station:Station) -> some View {
        HStack {
            VStack {
                switch viewState.currentlyPlaying {
                case let .playing(songname: _, on: s): s.id == station.id
                    ? Text("\(Image(systemName:labels.play)) \(station.name)")
                    : Text(                                 "\(station.name)")
                case let .paused(s): s.id == station.id
                    ? Text("\(Image(systemName:labels.pause)) \(station.name)")
                    : Text(                                 " \(station.name)")
                default: Text("\(station.name)")
                }
            }
            Spacer()
            Button {
                viewState.favoriteStations.contains(station)
                ? viewState.roothandler(.radio(.remove(station,from:.favorites)))
                : viewState.roothandler(.radio(.add   (station,to  :.favorites)))
            } label: { Image(systemName:viewState.favoriteStations.contains(station) ? icons.favourite.selected : icons.favourite.notSelected)}
        }.stroke()
    }
    func stations() -> some View {
        let currentStation:Station?
        switch viewState.currentlyPlaying {
        case .nonePlaying: currentStation = nil
        case .playing(songname: _, on: let s): currentStation = s
        case .paused(let s): currentStation = s
        }
        return VStack {
            List {
                ForEach([(0,labels.searchStations)], id:\.0) {
                    TextField($0.1,text:$searchText)
                        .modifier(ClearButton(text:$searchText){ searchIsFocused = false })
                        .background(.clear)
                        .focused($searchIsFocused)
                }
                .listRowBackground(colors.bg.list.row)
                if !isInSearchMode {
                    Section {
                        ForEach(viewState.favoriteStations + (
                            currentStation != nil && !viewState.favoriteStations.contains(currentStation!)
                                 ? [currentStation!]
                                 : [])
                        ) { station in
                            Button {
                                viewState.roothandler(.radio(.play(station)))
                                viewState.roothandler(.radio(.visualizer(.start)))
                            } label: {
                                listItemFor(station)
                            }
                        }
                        .listRowBackground(colors.bg.list.row)
                    }
                }
                Section {
                    ForEach(isInSearchMode ? viewState.filteredStations : []) { station in
                        Button {
                            viewState.roothandler(.radio(.play(station)))
                            viewState.roothandler(.radio(.visualizer(.start)))
                            searchIsFocused = false
                        } label: {
                            listItemFor(station)
                        }
                    }
                }
                .listRowBackground(colors.bg.list.row)
            }
            .padding(0)
            .scrollContentBackground(.hidden)
            .background(.clear)
            .listStyle(InsetGroupedListStyle())
        }
        .padding(0)
        .background(Color.clear)
        .onChange(of:searchText) { 
            viewState.roothandler(.radio(.filter(by:.name($0))))
        }
    }
    func info() -> some View { EmptyView() }
    func miniplayer() -> some View {
        HStack {
            switch viewState.currentlyPlaying {
            case let .playing(songname:song,on:station): playingButton(for:station,song:song)
            case let .paused(station)                  :   pauseButton(for:station          )
            case     .nonePlaying                      : EmptyView()                        }
        }
        .padding()
    }
    func playingButton(for station:Station,song:String) -> some View {
        VStack {
            ForEach(
                [
                    song != "" ? (0,icons.playing.artist, artist(from:song)) : nil,
                    song != "" ? (1,icons.playing.song,     name(from:song)) : nil,
                                 (2,icons.playing.station, station.name)
                ].compactMap{ $0 }, id:\.0) { pair in
                    LazyVGrid(columns: [
                        GridItem(.fixed   (40),         spacing:10,         alignment:.center ),
                        GridItem(.flexible(minimum:200, maximum:.infinity), alignment:.leading)
                    ]) {
                        Image(systemName:pair.1).stroke().frame(   width: 40,    height:32, alignment:.leading).padding(.leading,   16)
                        Text (           pair.2).stroke().frame(minWidth:200, maxHeight:32, alignment:.leading).minimumScaleFactor(0.5)
                    }
            }
        }
        .padding()
        .background(colors.bg.button.play)
        .cornerRadius(16)
        .onTapGesture {
            viewState.roothandler(.radio(.pause))
        }
    }
    func pauseButton(for station:Station) -> some View {
        VStack {
            ForEach([ (0,icons.playing.station, station.name) ], id:\.0)
                { pair in
                    LazyVGrid(columns: [
                        GridItem(.fixed   (40),         spacing:10,         alignment:.center ),
                        GridItem(.flexible(minimum:200, maximum:.infinity), alignment:.leading)
                    ]) {
                        ZStack {
                            Image(systemName:pair.1).opacity(0.5)
                            Image(systemName:icons.pause.filled)
                        }.frame(width: 40,height:32, alignment:.leading).padding(.leading,16)
                        Text (pair.2).stroke().frame(minWidth:200, maxHeight:32, alignment:.leading).minimumScaleFactor(0.5)
                    }
            }
        }
        .padding()
        .background(colors.bg.button.pause)
        .cornerRadius(16)
        .onTapGesture {
            viewState.roothandler(.radio(.play(station)))
        }
    }
}
fileprivate
func name(from text:String) -> String {
    guard text.contains("-") else { return text }
    return String(text.split(separator: "-")[1]).trimmingCharacters(in: .whitespacesAndNewlines)
}
fileprivate
func artist(from text:String) -> String {
    guard text.contains("-") else { return "" }
    return String(text.split(separator: "-")[0]).trimmingCharacters(in: .whitespacesAndNewlines)
}
