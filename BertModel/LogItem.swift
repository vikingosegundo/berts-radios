//
//  LogItem.swift
//  BrighterModel
//
//  Created by vikingosegundo on 24/11/2021.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

public struct LogItem {
    public let text:String
    public let date:Date
    public let logLevel: LogItem.LogLevel
    public enum LogLevel {
        case info
        case warn
        case error
        case custom(String)
    }
    public init (text t:String, date d:Date = Date(), logLevel l: LogItem.LogLevel = .info) { text = t; date = d; logLevel = l }
}
