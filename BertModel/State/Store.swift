//
//  StateHandler.swift
//  Todos
//
//  Created by Manuel Meyer on 12/04/2021.
//

public typealias Access<S> = (                    ) -> S
public typealias Change<C> = ( C...               ) -> ()
public typealias Reset     = (                    ) -> ()
public typealias Callback  = ( @escaping () -> () ) -> ()
public typealias Destroy   = (                    ) -> ()

public typealias Store<S,C> = (
      state: Access<S>,
     change: Change<C>,
      reset: Reset,
    updated: Callback,
    destroy: Destroy
)

public func state  <S,C>(in store:Store<S,C>          ) -> S { store.state()               }
public func change <S,C>(_  store:Store<S,C>,_ cs:C...)      { cs.forEach{store.change($0)}}
public func reset  <S,C>(_  store:Store<S,C>          )      { store.reset()               }
public func destroy<S,C>(_  store:inout Store<S,C>!   )      { store.destroy();store = nil }
