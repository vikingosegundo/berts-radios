//
//  State.swift
//  Bert's Radios
//
//  Created by vikingosegundo on 15/12/2021.
//

import BertTools

public typealias AppStore = Store<AppState,AppState.Change>

public struct AppState: Codable {
    public enum Change {
        case add(Station, to:_Add); public enum _Add {
            case stations
            case favourites }
        case replace(_Replace); public enum _Replace {
            case stations       (with:[Station])
            case filtered       (with:[Station])
            case settingStations(with:[Station]) }
        case remove(Station, from:_From); public enum _From {
            case favourites }
        case current(Current)
        case   activate(_Activate)
        case deactivate(_Activate); public enum _Activate {
            case visualisation }
    }
    public init () { self.init([], [], [] ,[], .nonePlaying, true) }
    public let stations           : [Station]
    public let filteredStations   : [Station]
    public let favoriteStations   : [Station]
    public let settingsStations   : [Station]
    public let currentStation     : Current
    public let visualisationActive: Bool
    public func alter(_ changes: [Change] ) -> Self { changes.reduce(self) { $0.alter($1) } }
    public func alter(_ changes: Change...) -> Self { changes.reduce(self) { $0.alter($1) } }
}

private extension AppState {
    init (_            stations: [Station],
          _            filtered: [Station],
          _    favoriteStations: [Station],
          _    settingsStations: [Station],
          _      currentStation: Current  ,
          _ visualisationActive: Bool     )
    {
        self.stations            = stations
        self.filteredStations    = filtered
        self.currentStation      = currentStation
        self.favoriteStations    = favoriteStations
        self.settingsStations    = settingsStations
        self.visualisationActive = visualisationActive
    }
    func alter(_ change:Change) -> Self {
        switch change {
        case let .add    (station,           to:        .stations): return .init(stations + [station],filteredStations,favoriteStations,                        settingsStations,currentStation,visualisationActive)
        case let .replace(.stations       (with:        stations)): return .init(stations,            filteredStations,favoriteStations,                        settingsStations,currentStation,visualisationActive)
        case let .replace(.filtered       (with:filteredStations)): return .init(stations,            filteredStations,favoriteStations,                        settingsStations,currentStation,visualisationActive)
        case let .add    (station,           to:      .favourites): return .init(stations,            filteredStations,favoriteStations - [station] + [station],settingsStations,currentStation,visualisationActive)
        case let .remove (station,         from:      .favourites): return .init(stations,            filteredStations,favoriteStations - [station],            settingsStations,currentStation,visualisationActive)
        case let .replace(.settingStations(with:settingsStations)): return .init(stations,            filteredStations,favoriteStations,                        settingsStations,currentStation,visualisationActive)
        case let .current(                         currentStation): return .init(stations,            filteredStations,favoriteStations,                        settingsStations,currentStation,visualisationActive)
        case    .activate(.visualisation                         ): return .init(stations,            filteredStations,favoriteStations,                        settingsStations,currentStation,true               )
        case  .deactivate(.visualisation                         ): return .init(stations,            filteredStations,favoriteStations,                        settingsStations,currentStation,false              )
        }
    }
}
