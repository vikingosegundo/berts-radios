//
//  Message.swift
//  Bert's Radios
//
//  Created by vikingosegundo on 15/12/2021.
//

public enum Message {
    case radio(Radio)   // Radio station feature
    case settings(Settings)
    case logging(Logging)
}
//MARK: - Radio station feature
extension Message {
    public enum Radio {
        // load stations
        case load  (_Load)
        case loaded(_Load._Response)
        public enum _Load {
            case all(_All); public enum _All {
                case stations }
            public enum _Response {
                case all(_All, Result); public enum _All {
                    case stations([Station]) } } }
        
        // player operations
        case play   (Station)
        case waiting(Station)
        case playing(String, on:Station)
        case pause
        case paused (Station)
        
        // Favourites
        case add(Station, to:_To); public enum _To {
            case favorites }
        case remove(Station, from:_From); public enum _From {
            case favorites }
        case favorite(_Favorite, for:Station); public enum _Favorite{
            case was(_Was); public enum _Was {
                case added
                case removed } }
        
        // filter shown stations
        case filter  (by:_Filter)
        case filtered(by:_Filter._Response)
        case clearFilter
        case filterCleard
        public enum _Filter {
            case name(String)
            public enum _Response {
                case name(String, stations:[Station]) } }
        
        // visualizing sound
        case visualizer(_Visualizer); public enum _Visualizer {
            case   activate
            case deactivate
            case start
            case stop
            case loudness(interpolated:[Float])
            
            case activation(_Result)
            case deactivation(_Result)

            public enum _Result {
                case succeeded
            }
        }
    }
}
//MARK: - Settings "feature"
extension Message {
    public enum Settings {
        public enum _Fetch {
            case all     (_All       ); public enum _All { case stations }
            case stations(FilterRules)
        }
        case fetch  (_Fetch)
        case fetched(_Fetch, [Station])
    }
}
//MARK: - Common DSL
public enum Result {
    case successfully
    case failed
}
public enum FilterRules {
    case starting(with:String)
    case containing(String)
}
public enum Filter {
    case title(FilterRules)
}
public extension Message {
    enum Logging { // Logging Feature
    //      |-- Command --|
        case write(LogItem)
    }
}
