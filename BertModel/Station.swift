//
//  Station.swift
//  Bert's Radios
//
//  Created by vikingosegundo on 15/12/2021.
//

import Foundation

public struct Station:Codable, Identifiable, Equatable, Hashable {
    public enum Change {
        case add(_Add); public enum _Add {
            case url(String)
        }
    }
    public struct URL:Codable, Equatable, Hashable {
        public let address:String
        public init(address a:String){ address = a }
    }
    public let id  : UUID
    public let name: String
    public let urls: [Station.URL]
    
    public init(name: String) {
        self.init(UUID(),name, [])
    }
    public func alter(_ cs:Change...) -> Self { cs.reduce(self) { $0.alter($1) } }
}

private extension Station {
    private init(_ id:UUID,_ name: String, _ urls:[Station.URL]) {
        self.id = id
        self.name = name
        self.urls = urls
    }
    private func alter(_ c:Change) -> Self {
        switch c {
        case let .add(.url(address)): return .init(id, name, urls.filter({ $0.address != address }) + [.init(address:address)])
        }
    }
}
