//
//  Current.swift
//  Bert's Radios
//
//  Created by vikingosegundo on 17/12/2021.
//

public enum Current: Codable, Equatable {
    case nonePlaying
    case playing(songname:String, on:Station)
    case paused (Station)
}
public func station(from current:Current) -> Station? {
    switch current {
    case .playing(_, let s): return s
    case .paused (   let s): return s
    case .nonePlaying      : return nil
    }
}
