//
//  AppSpec.swift
//  Bert's RadiosTests
//
//  Created by Manuel Meyer on 02.05.22.
//

import Quick
import Nimble
import AudioStreaming
import BertModel
import BertsApp
import BertUI
@testable
import Bert_s_Radios

class AppSpec:QuickSpec {
    override func spec() {
        context("Bert's Stations") {
            var app: Input!
            var store:AppStore?
            var viewState:ViewState!
            beforeSuite { deletePossilblePreviousStore()                                        }
            beforeEach  { store     = createDiskStore(pathInDocs:storeName)                     }; afterEach { destroy(&store) }
            beforeEach  { viewState = ViewState(store:store!, roothandler:{app?($0)})           }; afterEach { viewState = nil }
            beforeEach  { app       = createApp(store!, [viewState.handle(msg:)], { app?($0) }) }; afterEach { app       = nil }

            describe("app") {
                context("just created") {
                    it("has no stations in store"                ) { expect(store!.state()        .stations).to          (haveCount(0))        }
                    it("has no stations in viewstate"            ) { expect(viewState             .stations).toEventually(haveCount(0))        }
                    it("has no filtered stations in store"       ) { expect(store!.state().filteredStations).to          (haveCount(0))        }
                    it("has no filtered stations in viewstate"   ) { expect(viewState     .filteredStations).toEventually(haveCount(0))        }
                    it("has no favourite stations in store"      ) { expect(store!.state().favoriteStations).to          (haveCount(0))        }
                    it("has no favourite stations in viewstate"  ) { expect(viewState     .favoriteStations).toEventually(haveCount(0))        }
                    it("has no unfavourite stations in viewstate") { expect(viewState   .unfavoriteStations).toEventually(haveCount(0))        }
                    it("has no stations in store"                ) { expect(store!.state().settingsStations).to          (haveCount(0))        }
                    it("has no current station showing"          ) { expect(viewState     .currentlyPlaying).toEventually(equal(.nonePlaying)) }
                    it("has no current loudness showing"         ) { expect(viewState     .loudness        ).toEventually(beEmpty())           }
                    it("has it's cursor set to 0"                ) { expect(viewState     .cursor          ).toEventually(equal(0))            }
                    it("has it's visualisation activated"        ) { expect(viewState  .visualisationActive).toEventually(beTrue())            }
                }
                context("load stations") {
                    beforeEach {
                        app(.radio(.load(.all(.stations))))
                    }
                    it("has 4 stations in store"                ) { expect(store!.state()        .stations).to          (haveCount(4))        }
                    it("has 4 stations in viewstate"            ) { expect(viewState             .stations).toEventually(haveCount(4))        }
                    it("has no filtered stations in store"      ) { expect(store!.state().filteredStations).to          (haveCount(0))        }
                    it("has no filtered stations in viewstate"  ) { expect(viewState     .filteredStations).toEventually(haveCount(0))        }
                    it("has no favourite stations in store"     ) { expect(store!.state().favoriteStations).to          (haveCount(0))        }
                    it("has no favourite stations in viewstate" ) { expect(viewState     .favoriteStations).toEventually(haveCount(0))        }
                    it("has 4 unfavourite stations in viewstate") { expect(viewState   .unfavoriteStations).toEventually(haveCount(4))        }
                    it("has no stations in settings"            ) { expect(store!.state().settingsStations).to          (haveCount(0))        }
                    it("has no current station showing"         ) { expect(viewState     .currentlyPlaying).toEventually(equal(.nonePlaying)) }
                    it("has no current loudness showing"        ) { expect(viewState     .loudness        ).toEventually(beEmpty())           }
                    it("has it's cursor set to 0"               ) { expect(viewState     .cursor          ).toEventually(equal(0))            }
                    it("has it's visualisation activated"       ) { expect(viewState  .visualisationActive).toEventually(beTrue())            }
                    context("add favorite station") {
                        beforeEach {
                            app(.radio(.add(store!.state().stations.first!,to:.favorites)))
                        }
                        it("has 4 stations in store"                ) { expect(store!.state()        .stations).to          (haveCount(4))        }
                        it("has 4 stations in viewstate"            ) { expect(viewState             .stations).toEventually(haveCount(4))        }
                        it("has no filtered stations in store"      ) { expect(store!.state().filteredStations).to          (haveCount(0))        }
                        it("has no filtered stations in viewstate"  ) { expect(viewState     .filteredStations).toEventually(haveCount(0))        }
                        it("has 1 favourite stations in store"      ) { expect(store!.state().favoriteStations).to          (haveCount(1))        }
                        it("has 1 favourite stations in viewstate"  ) { expect(viewState     .favoriteStations).toEventually(haveCount(1))        }
                        it("has 3 unfavourite stations in viewstate") { expect(viewState   .unfavoriteStations).toEventually(haveCount(3))        }
                        it("has no stations in settings"            ) { expect(store!.state().settingsStations).to          (haveCount(0))        }
                        it("has no current station showing"         ) { expect(viewState     .currentlyPlaying).toEventually(equal(.nonePlaying)) }
                        it("has no current loudness showing"        ) { expect(viewState     .loudness        ).toEventually(beEmpty())           }
                        it("has it's cursor set to 0"               ) { expect(viewState     .cursor          ).toEventually(equal(0))            }
                        it("has it's visualisation activated"       ) { expect(viewState  .visualisationActive).toEventually(beTrue())            }
                        context("add same favorite station") {
                            beforeEach {
                                app(.radio(.add(store!.state().stations.first!,to:.favorites)))
                            }
                            it("has 4 stations in store"                ) { expect(store!.state()        .stations).to          (haveCount(4))        }
                            it("has 4 stations in viewstate"            ) { expect(viewState             .stations).toEventually(haveCount(4))        }
                            it("has no filtered stations in store"      ) { expect(store!.state().filteredStations).to          (haveCount(0))        }
                            it("has no filtered stations in viewstate"  ) { expect(viewState     .filteredStations).toEventually(haveCount(0))        }
                            it("has 1 favourite stations in store"      ) { expect(store!.state().favoriteStations).to          (haveCount(1))        }
                            it("has 1 favourite stations in viewstate"  ) { expect(viewState     .favoriteStations).toEventually(haveCount(1))        }
                            it("has 3 unfavourite stations in viewstate") { expect(viewState   .unfavoriteStations).toEventually(haveCount(3))        }
                            it("has no stations in settings"            ) { expect(store!.state().settingsStations).to          (haveCount(0))        }
                            it("has no current station showing"         ) { expect(viewState     .currentlyPlaying).toEventually(equal(.nonePlaying)) }
                            it("has no current loudness showing"        ) { expect(viewState     .loudness        ).toEventually(beEmpty())           }
                            it("has it's cursor set to 0"               ) { expect(viewState     .cursor          ).toEventually(equal(0))            }
                            it("has it's visualisation activated"       ) { expect(viewState  .visualisationActive).toEventually(beTrue())            }
                        }
                        context("add another favorite station") {
                            beforeEach {
                                app(.radio(.add(store!.state().stations.last!,to:.favorites)))
                            }
                            it("has 4 stations in store"                ) { expect(store!.state()        .stations).to          (haveCount(4))        }
                            it("has 4 stations in viewstate"            ) { expect(viewState             .stations).toEventually(haveCount(4))        }
                            it("has no filtered stations in store"      ) { expect(store!.state().filteredStations).to          (haveCount(0))        }
                            it("has no filtered stations in viewstate"  ) { expect(viewState     .filteredStations).toEventually(haveCount(0))        }
                            it("has 2 favourite stations in store"      ) { expect(store!.state().favoriteStations).to          (haveCount(2))        }
                            it("has 2 favourite stations in viewstate"  ) { expect(viewState     .favoriteStations).toEventually(haveCount(2))        }
                            it("has 2 unfavourite stations in viewstate") { expect(viewState   .unfavoriteStations).toEventually(haveCount(2))        }
                            it("has no stations in settings"            ) { expect(store!.state().settingsStations).to          (haveCount(0))        }
                            it("has no current station showing"         ) { expect(viewState     .currentlyPlaying).toEventually(equal(.nonePlaying)) }
                            it("has no current loudness showing"        ) { expect(viewState     .loudness        ).toEventually(beEmpty())           }
                            it("has it's cursor set to 0"               ) { expect(viewState     .cursor          ).toEventually(equal(0))            }
                            it("has it's visualisation activated"       ) { expect(viewState  .visualisationActive).toEventually(beTrue())            }
                        }
                        context("remove favorite station") {
                            beforeEach {
                                app(.radio(.remove(store!.state().stations.first!,from:.favorites)))
                            }
                            it("has 4 stations in store"                ) { expect(store!.state()        .stations).to          (haveCount(4))        }
                            it("has 4 stations in viewstate"            ) { expect(viewState             .stations).toEventually(haveCount(4))        }
                            it("has no filtered stations in store"      ) { expect(store!.state().filteredStations).to          (haveCount(0))        }
                            it("has no filtered stations in viewstate"  ) { expect(viewState     .filteredStations).toEventually(haveCount(0))        }
                            it("has 0 favourite stations in store"      ) { expect(store!.state().favoriteStations).to          (haveCount(0))        }
                            it("has 0 favourite stations in viewstate"  ) { expect(viewState     .favoriteStations).toEventually(haveCount(0))        }
                            it("has 4 unfavourite stations in viewstate") { expect(viewState   .unfavoriteStations).toEventually(haveCount(4))        }
                            it("has no stations in settings"            ) { expect(store!.state().settingsStations).to          (haveCount(0))        }
                            it("has no current station showing"         ) { expect(viewState     .currentlyPlaying).toEventually(equal(.nonePlaying)) }
                            it("has no current loudness showing"        ) { expect(viewState     .loudness        ).toEventually(beEmpty())           }
                            it("has it's cursor set to 0"               ) { expect(viewState     .cursor          ).toEventually(equal(0))            }
                            it("has it's visualisation activated"       ) { expect(viewState  .visualisationActive).toEventually(beTrue())            }
                        }
                        context("filter stations for «c»") {
                            beforeEach {
                                app(.radio(.filter(by:.name("c"))))
                            }
                            it("has 4 stations in store"                ) { expect(store!.state()        .stations).to          (haveCount(4))        }
                            it("has 4 stations in viewstate"            ) { expect(viewState             .stations).toEventually(haveCount(4))        }
                            it("has 2 filtered station in store"        ) { expect(store!.state().filteredStations).to          (haveCount(2))        }
                            it("has 2 filtered station in viewstate"    ) { expect(viewState     .filteredStations).toEventually(haveCount(2))        }
                            it("has 1 favourite stations in store"      ) { expect(store!.state().favoriteStations).to          (haveCount(1))        }
                            it("has 1 favourite stations in viewstate"  ) { expect(viewState     .favoriteStations).toEventually(haveCount(1))        }
                            it("has 3 unfavourite stations in viewstate") { expect(viewState   .unfavoriteStations).toEventually(haveCount(3))        }
                            it("has no stations in settings"            ) { expect(store!.state().settingsStations).to          (haveCount(0))        }
                            it("has no current station showing"         ) { expect(viewState     .currentlyPlaying).toEventually(equal(.nonePlaying)) }
                            it("has no current loudness showing"        ) { expect(viewState     .loudness        ).toEventually(beEmpty())           }
                            it("has it's cursor set to 0"               ) { expect(viewState     .cursor          ).toEventually(equal(0))            }
                            it("has it's visualisation activated"       ) { expect(viewState  .visualisationActive).toEventually(beTrue())            }
                            context("filter further for «co»") {
                                beforeEach {
                                    app(.radio(.filter(by:.name("co"))))
                                }
                                it("has 4 stations in store"                ) { expect(store!.state()        .stations).to          (haveCount(4))        }
                                it("has 4 stations in viewstate"            ) { expect(viewState             .stations).toEventually(haveCount(4))        }
                                it("has 1 filtered station in store"        ) { expect(store!.state().filteredStations).to          (haveCount(1))        }
                                it("has 1 filtered station in viewstate"    ) { expect(viewState     .filteredStations).toEventually(haveCount(1))        }
                                it("has 1 favourite stations in store"      ) { expect(store!.state().favoriteStations).to          (haveCount(1))        }
                                it("has 1 favourite stations in viewstate"  ) { expect(viewState     .favoriteStations).toEventually(haveCount(1))        }
                                it("has 3 unfavourite stations in viewstate") { expect(viewState   .unfavoriteStations).toEventually(haveCount(3))        }
                                it("has no stations in settings"            ) { expect(store!.state().settingsStations).to          (haveCount(0))        }
                                it("has no current station showing"         ) { expect(viewState     .currentlyPlaying).toEventually(equal(.nonePlaying)) }
                                it("has no current loudness showing"        ) { expect(viewState     .loudness        ).toEventually(beEmpty())           }
                                it("has it's cursor set to 0"               ) { expect(viewState     .cursor          ).toEventually(equal(0))            }
                                it("has it's visualisation activated"       ) { expect(viewState  .visualisationActive).toEventually(beTrue())            }
                                context("filter further for «cosmos»") {
                                    beforeEach {
                                        app(.radio(.filter(by:.name("cosmos"))))
                                    }
                                    it("has 4 stations in store"                ) { expect(store!.state()        .stations).to          (haveCount(4))        }
                                    it("has 4 stations in viewstate"            ) { expect(viewState             .stations).toEventually(haveCount(4))        }
                                    it("has 0 filtered station in store"        ) { expect(store!.state().filteredStations).to          (haveCount(0))        }
                                    it("has 0 filtered station in viewstate"    ) { expect(viewState     .filteredStations).toEventually(haveCount(0))        }
                                    it("has 1 favourite stations in store"      ) { expect(store!.state().favoriteStations).to          (haveCount(1))        }
                                    it("has 1 favourite stations in viewstate"  ) { expect(viewState     .favoriteStations).toEventually(haveCount(1))        }
                                    it("has 3 unfavourite stations in viewstate") { expect(viewState   .unfavoriteStations).toEventually(haveCount(3))        }
                                    it("has no stations in settings"            ) { expect(store!.state().settingsStations).to          (haveCount(0))        }
                                    it("has no current station showing"         ) { expect(viewState     .currentlyPlaying).toEventually(equal(.nonePlaying)) }
                                    it("has no current loudness showing"        ) { expect(viewState     .loudness        ).toEventually(beEmpty())           }
                                    it("has it's cursor set to 0"               ) { expect(viewState     .cursor          ).toEventually(equal(0))            }
                                    it("has it's visualisation activated"       ) { expect(viewState  .visualisationActive).toEventually(beTrue())            }
                                }
                                context("filter for «COSMO»") {
                                    beforeEach {
                                        app(.radio(.filter(by:.name("COSMO"))))
                                    }
                                    it("has 4 stations in store"                ) { expect(store!.state()        .stations).to          (haveCount(4))        }
                                    it("has 4 stations in viewstate"            ) { expect(viewState             .stations).toEventually(haveCount(4))        }
                                    it("has 1 filtered station in store"        ) { expect(store!.state().filteredStations).to          (haveCount(1))        }
                                    it("has 1 filtered station in viewstate"    ) { expect(viewState     .filteredStations).toEventually(haveCount(1))        }
                                    it("has 1 favourite stations in store"      ) { expect(store!.state().favoriteStations).to          (haveCount(1))        }
                                    it("has 1 favourite stations in viewstate"  ) { expect(viewState     .favoriteStations).toEventually(haveCount(1))        }
                                    it("has 3 unfavourite stations in viewstate") { expect(viewState   .unfavoriteStations).toEventually(haveCount(3))        }
                                    it("has no stations in settings"            ) { expect(store!.state().settingsStations).to          (haveCount(0))        }
                                    it("has no current station showing"         ) { expect(viewState     .currentlyPlaying).toEventually(equal(.nonePlaying)) }
                                    it("has no current loudness showing"        ) { expect(viewState     .loudness        ).toEventually(beEmpty())           }
                                    it("has it's cursor set to 0"               ) { expect(viewState     .cursor          ).toEventually(equal(0))            }
                                    it("has it's visualisation activated"       ) { expect(viewState  .visualisationActive).toEventually(beTrue())            }
                                }
                                context("clear filter") {
                                    beforeEach {
                                        app(.radio(.clearFilter))
                                    }
                                    it("has 4 stations in store"                ) { expect(store!.state()        .stations).to          (haveCount(4))        }
                                    it("has 4 stations in viewstate"            ) { expect(viewState             .stations).toEventually(haveCount(4))        }
                                    it("has 0 filtered station in store"        ) { expect(store!.state().filteredStations).to          (haveCount(0))        }
                                    it("has 0 filtered station in viewstate"    ) { expect(viewState     .filteredStations).toEventually(haveCount(0))        }
                                    it("has 1 favourite stations in store"      ) { expect(store!.state().favoriteStations).to          (haveCount(1))        }
                                    it("has 1 favourite stations in viewstate"  ) { expect(viewState     .favoriteStations).toEventually(haveCount(1))        }
                                    it("has 3 unfavourite stations in viewstate") { expect(viewState   .unfavoriteStations).toEventually(haveCount(3))        }
                                    it("has no stations in settings"            ) { expect(store!.state().settingsStations).to          (haveCount(0))        }
                                    it("has no current station showing"         ) { expect(viewState     .currentlyPlaying).toEventually(equal(.nonePlaying)) }
                                    it("has no current loudness showing"        ) { expect(viewState     .loudness        ).toEventually(beEmpty())           }
                                    it("has it's cursor set to 0"               ) { expect(viewState     .cursor          ).toEventually(equal(0))            }
                                    it("has it's visualisation activated"       ) { expect(viewState  .visualisationActive).toEventually(beTrue())            }
                                }
                            }
                        }
                    }
                    context("play") {
                        beforeEach { app(.radio(.play(store!.state().stations.last!))) }
                         afterEach { app(.radio(.pause)) }
                        it("has 4 stations in store"                ) { expect(store!.state()        .stations).toEventually(haveCount(4), timeout:.seconds(60))        }
                        it("has 4 stations in viewstate"            ) { expect(viewState             .stations).toEventually(haveCount(4), timeout:.seconds(60))        }
                        it("has no filtered stations in store"      ) { expect(store!.state().filteredStations).toEventually(haveCount(0), timeout:.seconds(60))        }
                        it("has no filtered stations in viewstate"  ) { expect(viewState     .filteredStations).toEventually(haveCount(0), timeout:.seconds(60))        }
                        it("has no favourite stations in store"     ) { expect(store!.state().favoriteStations).toEventually(haveCount(0), timeout:.seconds(60))        }
                        it("has no favourite stations in viewstate" ) { expect(viewState     .favoriteStations).toEventually(haveCount(0), timeout:.seconds(60))        }
                        it("has 4 unfavourite stations in viewstate") { expect(viewState   .unfavoriteStations).toEventually(haveCount(4), timeout:.seconds(60))        }
                        it("has no stations in store"               ) { expect(store!.state().settingsStations).toEventually(haveCount(0), timeout:.seconds(60))        }
                        it("has no current station showing"         ) { expect(isPlaying(station:store!.state().stations.last!, in:viewState.currentlyPlaying)).toEventually(beTrue(), timeout:.seconds(60)) }
                        it("has no current loudness showing"        ) { expect(viewState     .loudness        ).toEventually(beEmpty(), timeout:.seconds(60)  )           }
                        it("has it's cursor set to 0"               ) { expect(viewState     .cursor          ).toEventually(equal(0) , timeout:.seconds(60)  )            }
                        it("has it's visualisation activated"       ) { expect(viewState  .visualisationActive).toEventually(beTrue() , timeout:.seconds(60)  )            }
                    }
                    context("settings") {
                        context("manage station") {
                            beforeEach {
                                app(.settings(.fetch(.all(.stations))))
                            }
                            it("has 4 stations in settings"             ) { expect(store!.state().settingsStations).to          (haveCount(4))        }
                            it("has 4 stations in store"                ) { expect(store!.state()        .stations).to          (haveCount(4))        }
                            it("has 4 stations in viewstate"            ) { expect(viewState             .stations).toEventually(haveCount(4))        }
                            it("has no filtered stations in store"      ) { expect(store!.state().filteredStations).to          (haveCount(0))        }
                            it("has no filtered stations in viewstate"  ) { expect(viewState     .filteredStations).toEventually(haveCount(0))        }
                            it("has no favourite stations in store"     ) { expect(store!.state().favoriteStations).to          (haveCount(0))        }
                            it("has no favourite stations in viewstate" ) { expect(viewState     .favoriteStations).toEventually(haveCount(0))        }
                            it("has 4 unfavourite stations in viewstate") { expect(viewState   .unfavoriteStations).toEventually(haveCount(4))        }
                            it("has no current station showing"         ) { expect(viewState     .currentlyPlaying).toEventually(equal(.nonePlaying)) }
                            it("has no current loudness showing"        ) { expect(viewState     .loudness        ).toEventually(beEmpty())           }
                            it("has it's cursor set to 0"               ) { expect(viewState     .cursor          ).toEventually(equal(0))            }
                            it("has it's visualisation activated"       ) { expect(viewState  .visualisationActive).toEventually(beTrue())            }
                        }
                    }
                }
            }
            func deletePossilblePreviousStore() {
                store = createDiskStore(pathInDocs:storeName)
                destroy(&store)
            }
        }
    }
}

fileprivate func isPlaying(station:Station,in current:Current) -> Bool {
    switch current {
    case let .playing(_, on:s):
        return station.id == s.id
    default: return false
    }
}
fileprivate let createApp = createAppDomain
fileprivate let storeName = "specs.json"
