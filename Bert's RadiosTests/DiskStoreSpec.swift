//
//  DiskStoreSpec.swift
//  Bert's RadiosTests
//
//  Created by vikingosegundo on 12/01/2022.
//
import Foundation
import Quick
import Nimble
import AudioStreaming
import BertModel

@testable import Bert_s_Radios

class DiskStoreSpec:QuickSpec {
    override func spec() {
        var store:AppStore!; beforeEach { store = createDiskStore(pathInDocs:"DiskStoreSpec.json") }; afterEach { destroy(&store) }
        context("DiskStore") {
            context("creation") {
                it("with empty station list"  ) { expect(store.state().stations        ).to(beEmpty()) }
                it("with empty favourite list") { expect(store.state().favoriteStations).to(beEmpty()) }
                context("adding") {
                    beforeEach {
                        change(store,.add(Station(name:"KKBL"),          to:.stations  ))
                        change(store,.add(store.state().stations.first!, to:.favourites)) }
                    it("a station is added"  ) { expect(store.state().stations        .first?.name).to(equal("KKBL"))}
                    it("a favourite is added") { expect(store.state().favoriteStations.first?.name).to(equal("KKBL"))}
                    context("resetting") {
                        beforeEach { reset(store) }
                        it("with empty station list" ) { expect(store.state().stations        ).to(beEmpty()) }
                        it("with empty fvourite list") { expect(store.state().favoriteStations).to(beEmpty()) } } } }
            context("observing") {
                var wasUpdated = false
                beforeEach { store.updated { wasUpdated = true } }
                 afterEach {                 wasUpdated = false  }
                context("adding new station"             ) { beforeEach { store.change(.add(Station(name:"frühstyxradio"), to:.stations  ))        }; it("will inform listeners") { expect(wasUpdated).to(beTrue()) } }
                context("adding new favourite"           ) { beforeEach { store.change(.add(Station(name:"frühstyxradio"), to:.favourites))        }; it("will inform listeners") { expect(wasUpdated).to(beTrue()) } }
                context("replacing stations"             ) { beforeEach { store.change(.replace(.stations(with:[])))                               }; it("will inform listeners") { expect(wasUpdated).to(beTrue()) } }
                context("replacing filtered stations"    ) { beforeEach { store.change(.replace(.filtered(with:[])))                               }; it("will inform listeners") { expect(wasUpdated).to(beTrue()) } }
                context("setting current playing station") { beforeEach { store.change(.current(.playing(songname:"111", on:Station(name:"111")))) }; it("will inform listeners") { expect(wasUpdated).to(beTrue()) } }
                context("setting current paused station" ) { beforeEach { store.change(.current(.paused(Station(name:"111"))))                     }; it("will inform listeners") { expect(wasUpdated).to(beTrue()) } }
                context("setting current none station"   ) { beforeEach { store.change(.current(.nonePlaying))                                     }; it("will inform listeners") { expect(wasUpdated).to(beTrue()) } }
            }
        }
    }
}
