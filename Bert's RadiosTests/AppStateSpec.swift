//
//  AppStateSpec.swift
//  Bert's RadiosTests
//
//  Created by vikingosegundo on 30/12/2021.
//

import Quick
import Nimble
import BertModel
@testable import Bert_s_Radios
import Darwin
import UIKit

class AppStateSpec: QuickSpec {
    override func spec() {
        describe("AppState") {
            let state = AppState()
            let station = Station(name:"KBBL").alter(.add(.url("https://kbbl.com")))
            context("adding station") {
                let orig = state
                let state = state.alter(.add(station, to:.stations))
                it("will be saved into stations"         ) { expect(state.stations           ) == [station]                           }
                it("will change stations"                ) { expect(state.stations           ).toNot(equal(orig.stations))            }
                it("will not save it as a favorite"      ) { expect(state.favoriteStations   ).to   (beEmpty())                       }
                it("will not change favorites"           ) { expect(state.favoriteStations   ).to   (equal(orig.favoriteStations))    }
                it("will not change filtered"            ) { expect(state.filteredStations   ).to   (equal(orig.filteredStations))    }
                it("will not change stations in settings") { expect(state.settingsStations   ).to   (equal(orig.settingsStations))    }
                it("will not change visualisationActive" ) { expect(state.visualisationActive).to   (equal(orig.visualisationActive)) }
                context("as favorite") {
                    let orig = state
                    let state = state.alter(.add(station, to:.favourites))
                    it("will be saved as a favorite"         ) { expect(state.favoriteStations   ) == [station]                           }
                    it("will change favorites"               ) { expect(state.favoriteStations   ).toNot(equal(orig.favoriteStations))    }
                    it("will not change stations"            ) { expect(state.stations           ).to   (equal(orig.stations))            }
                    it("will not change filtered"            ) { expect(state.filteredStations   ).to   (equal(orig.filteredStations))    }
                    it("will not change stations in settings") { expect(state.settingsStations   ).to   (equal(orig.settingsStations))    }
                    it("will not change visualisationActive" ) { expect(state.visualisationActive).to   (equal(orig.visualisationActive)) }
                    context("and then removing it")             {
                        let orig = state
                        let state = state.alter(.remove(station, from:.favourites))
                        it("will leave an empty favorite list"   ) { expect(state.favoriteStations   ).to   (beEmpty())                       }
                        it("will change favorites"               ) { expect(state.favoriteStations   ).toNot(equal(orig.favoriteStations))    }
                        it("will not change stations"            ) { expect(state.stations           ).to   (equal(orig.stations))            }
                        it("will not change filtered"            ) { expect(state.filteredStations   ).to   (equal(orig.filteredStations))    }
                        it("will not change stations in settings") { expect(state.settingsStations   ).to   (equal(orig.settingsStations))    }
                        it("will not change visualisationActive" ) { expect(state.visualisationActive).to   (equal(orig.visualisationActive)) }
                    }
                }
                context("replacing") {
                    context("stations") {
                        context("with empty list") {
                            let orig = state
                            let state = state.alter(.replace(.stations(with: [])))
                            it("will contain no stations"            ) { expect(state.stations           ).to   (beEmpty())                       }
                            it("will change stations"                ) { expect(state.stations           ).toNot(equal(orig.stations))            }
                            it("will not change favorites"           ) { expect(state.favoriteStations   ).to   (equal(orig.favoriteStations))    }
                            it("will not change filtered"            ) { expect(state.filteredStations   ).to   (equal(orig.filteredStations))    }
                            it("will not change stations in settings") { expect(state.settingsStations   ).to   (equal(orig.settingsStations))    }
                            it("will not change visualisationActive" ) { expect(state.visualisationActive).to   (equal(orig.visualisationActive)) }
                        }
                    }
                }
            }
            context("filtered stations")           {
                let orig = state
                let state = state.alter(.replace(.filtered(with:[station])))
                it("will be accessible via state"        ) { expect(state.filteredStations   ) == [station]                           }
                it("will  change filtered"               ) { expect(state.filteredStations   ).toNot(equal(orig.filteredStations))    }
                it("will not change stations"            ) { expect(state.stations           ).to   (equal(orig.stations))            }
                it("will not change favorites"           ) { expect(state.favoriteStations   ).to   (equal(orig.favoriteStations))    }
                it("will not change stations in settings") { expect(state.settingsStations   ).to   (equal(orig.settingsStations))    }
                it("will not change visualisationActive" ) { expect(state.visualisationActive).to   (equal(orig.visualisationActive)) }
            }
            context("visualisation") {
                it("is active by default") { expect(state.visualisationActive).to(beTrue()) }
                context("deactivate") {
                    let orig = state
                    let state = state.alter(.deactivate(.visualisation))
                    it("deactivate"                          ) { expect(state.visualisationActive).to   (beFalse())                       }
                    it("will change visualisationActive"     ) { expect(state.visualisationActive).toNot(equal(orig.visualisationActive)) }
                    it("will not  change filtered"           ) { expect(state.filteredStations   ).to   (equal(orig.filteredStations))    }
                    it("will not change stations"            ) { expect(state.stations           ).to   (equal(orig.stations))            }
                    it("will not change favorites"           ) { expect(state.favoriteStations   ).to   (equal(orig.favoriteStations))    }
                    it("will not change stations in settings") { expect(state.settingsStations   ).to   (equal(orig.settingsStations))    }
                    context("activate again") {
                        let orig = state
                        let state = state.alter(.activate(.visualisation))
                        it("activate again"                      ) { expect(state.visualisationActive).to   (beTrue())                        }
                        it("will change visualisationActive"     ) { expect(state.visualisationActive).toNot(equal(orig.visualisationActive)) }
                        it("will not  change filtered"           ) { expect(state.filteredStations   ).to   (equal(orig.filteredStations))    }
                        it("will not change stations"            ) { expect(state.stations           ).to   (equal(orig.stations))            }
                        it("will not change favorites"           ) { expect(state.favoriteStations   ).to   (equal(orig.favoriteStations))    }
                        it("will not change stations in settings") { expect(state.settingsStations   ).to   (equal(orig.settingsStations))    }
                    }
                }
            }
        }
    }
}
