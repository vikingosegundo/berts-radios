//
//  StationSpec.swift
//  Bert's RadiosTests
//
//  Created by vikingosegundo on 30/12/2021.
//

import Quick
import Nimble
import BertModel
@testable import Bert_s_Radios

class StationSpec: QuickSpec {
    override func spec() {
        describe("Station") {
            let station = Station(name:"KBBL")
            context("adding url") {
                let station = station.alter(.add(.url("https://kbbl.com")))
                it("url will be saved into station") { expect(station.urls) == [Station.URL(address:"https://kbbl.com")] }
                context("adding another url") {
                    let station = station.alter(.add(.url("https://ffn.de")))
                    it("url will be added to station") { expect(station.urls) == [Station.URL(address:"https://kbbl.com"), Station.URL(address:"https://ffn.de")] }
                    context("adding another url") {
                        let station = station.alter(.add(.url("https://www1.wdr.de/radio/cosmo/index.html")))
                        it("url will be added to station") { expect(station.urls) == [Station.URL(address:"https://kbbl.com"), Station.URL(address:"https://ffn.de"), Station.URL(address:"https://www1.wdr.de/radio/cosmo/index.html")] }
                        context("adding same url") {
                            let station = station.alter(.add(.url("https://www1.wdr.de/radio/cosmo/index.html")))
                            it("url will be added to station") { expect(station.urls) == [Station.URL(address:"https://kbbl.com"), Station.URL(address:"https://ffn.de"), Station.URL(address:"https://www1.wdr.de/radio/cosmo/index.html")] }
                        }
                    }
                }
            }
        }
    }
}
