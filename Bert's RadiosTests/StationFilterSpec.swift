//
//  StationFilterSpec.swift
//  Bert's RadiosTests
//
//  Created by vikingosegundo on 26/12/2021.
//

import Quick
import Nimble
import BertModel
@testable import Bert_s_Radios
import BertsApp

class StationFilterSpec: QuickSpec {
    override func spec() {
        var stationFilter: StationFilter!
        var store        : AppStore!
        var responses    : [StationFilter.Response]!
        beforeEach {
            store = createDiskStore(pathInDocs:"StationFilterSpec.json")
            responses = []
            stationFilter = StationFilter(store:store) { responses.append($0) }
            store.change(
                .add(.init(name: "KBBL"), to: .stations),
                .add(.init(name: "ab"  ), to: .stations),
                .add(.init(name: "bb"  ), to: .stations))
        }
        afterEach {
            destroy(&store)
            responses     = []
            stationFilter = nil
        }
        describe("StationFilter") {
            context("search for 'B'") { beforeEach { stationFilter.request(to: .filter(by: .name("B"))) }
                it("returns 3 stations") { expect(store.state().filteredStations.count) == 3 }
                context("search for 'BB'") { beforeEach { stationFilter.request(to: .filter(by: .name("BB"))) }
                    it("returns 2 stations") { expect(store.state().filteredStations.count) == 2 }
                    context("search for 'BBL'") { beforeEach { stationFilter.request(to: .filter(by: .name("BBL"))) }
                        it("returns 1 station") { expect(store.state().filteredStations.count) == 1 }
                        context("search for 'BBLL'") { beforeEach { stationFilter.request(to: .filter(by: .name("BBLL"))) }
                            it("returns 0 stations") { expect(store.state().filteredStations.count) == 0 }
                        }
                    }
                }
                context("search for 'KBB'") { beforeEach { stationFilter.request(to:.filter(by: .name("KBB"))) }
                    it("returns 1 station") { expect(store.state().filteredStations.count) == 1 }
                    context("search for 'kbb'") { beforeEach { stationFilter.request(to:.filter(by: .name("kbb"))) }
                        it("returns 1 station") { expect(store.state().filteredStations.count) == 1 }
                    }
                }
            }
        }
    }
}
