//
//  ToolsSpec.swift
//  Bert's RadiosTests
//
//  Created by Manuel Meyer on 11.04.22.
//

import Quick
import Nimble

final class ToolsSpec: QuickSpec {
    override func spec() {
        context("padding") {
            context("non-empty string") {
                let testString = "123456"
                var s = ""
                context("left") {
                    context("to 8 chars"          ) { beforeEach { s = testString.padding(      toLength:8            ) }; it("will add two blanks"       ) { expect(s).to(equal("123456  ")) } }
                    context("to 8 chars with dots") { beforeEach { s = testString.padding(      toLength:8,withPad:".") }; it("will add two dots"         ) { expect(s).to(equal("123456..")) } }
                    context("to 5 chars"          ) { beforeEach { s = testString.padding(      toLength:5            ) }; it("will cut last character"   ) { expect(s).to(equal("12345"   )) } }
                    context("to 5 chars with dots") { beforeEach { s = testString.padding(      toLength:5,withPad:".") }; it("will cut last character"   ) { expect(s).to(equal("12345"   )) } }
                    context("to 3 chars"          ) { beforeEach { s = testString.padding(.left,toLength:3            ) }; it("will cut last 3 characters") { expect(s).to(equal("123"     )) } }
                    context("to 3 chars with dots") { beforeEach { s = testString.padding(.left,toLength:3,withPad:".") }; it("will cut last 3 characters") { expect(s).to(equal("123"     )) } }
                }
                context("right") {
                    context("to 8 chars"          ) { beforeEach { s = testString.padding(.right,toLength:8            ) }; it("will prepend two blanks"   ) { expect(s).to(equal("  123456")) } }
                    context("to 8 chars with dots") { beforeEach { s = testString.padding(.right,toLength:8,withPad:".") }; it("will prepend two dots"     ) { expect(s).to(equal("..123456")) } }
                    context("to 5 chars"          ) { beforeEach { s = testString.padding(.right,toLength:5            ) }; it("will cut last character"   ) { expect(s).to(equal("12345"   )) } }
                    context("to 5 chars with dots") { beforeEach { s = testString.padding(.right,toLength:5,withPad:".") }; it("will cut last 2 characters") { expect(s).to(equal("12345"   )) } }
                    context("to 3 chars"          ) { beforeEach { s = testString.padding(.right,toLength:3            ) }; it("will cut last 3 characters") { expect(s).to(equal("123"     )) } }
                    context("to 3 chars with dots") { beforeEach { s = testString.padding(.right,toLength:3,withPad:".") }; it("will cut last 3 characters") { expect(s).to(equal("123"     )) } }
                }
                context("center") {
                    context("to 8 chars"          ) { beforeEach { s = testString.padding(.center,toLength:8            ) }; it("will prepend and append two blanks"  ) { expect(s).to(equal(" 123456 " )) } }
                    context("to 8 chars with dots") { beforeEach { s = testString.padding(.center,toLength:8,withPad:".") }; it("will prepend and append two dots"    ) { expect(s).to(equal(".123456." )) } }
                    context("to 9 chars"          ) { beforeEach { s = testString.padding(.center,toLength:9            ) }; it("will prepend and append three blanks") { expect(s).to(equal(" 123456  ")) } }
                    context("to 9 chars with dots") { beforeEach { s = testString.padding(.center,toLength:9,withPad:".") }; it("will prepend and append three dots"  ) { expect(s).to(equal(".123456..")) } }
                    context("to 5 chars"          ) { beforeEach { s = testString.padding(.center,toLength:5            ) }; it("will cut last 1 character"           ) { expect(s).to(equal("12345"    )) } }
                    context("to 5 chars with dots") { beforeEach { s = testString.padding(.center,toLength:5,withPad:".") }; it("will cut last 1 characters"          ) { expect(s).to(equal("12345"    )) } }
                    context("to 3 chars"          ) { beforeEach { s = testString.padding(.center,toLength:3            ) }; it("will cut last 3 characters"          ) { expect(s).to(equal("123"      )) } }
                    context("to 3 chars with dots") { beforeEach { s = testString.padding(.center,toLength:3,withPad:".") }; it("will cut last 3 characters"          ) { expect(s).to(equal("123"      )) } }
                }
            }
            context("empty string") {
                let testString = ""
                var s = ""
                context("left") {
                    context("to 8 chars"          ) { beforeEach { s = testString.padding(      toLength:8            ) }; it("will add 8 blanks") { expect(s).to(equal("        ")) } }
                    context("to 8 chars with dots") { beforeEach { s = testString.padding(      toLength:8,withPad:".") }; it("will add 8 dots"  ) { expect(s).to(equal("........")) } }
                    context("to 5 chars"          ) { beforeEach { s = testString.padding(      toLength:5            ) }; it("will add 5 blanks") { expect(s).to(equal("     "   )) } }
                    context("to 5 chars with dots") { beforeEach { s = testString.padding(      toLength:5,withPad:".") }; it("will add 5 dots"  ) { expect(s).to(equal("....."   )) } }
                    context("to 3 chars"          ) { beforeEach { s = testString.padding(.left,toLength:3            ) }; it("will add 3 blanks") { expect(s).to(equal("   "     )) } }
                    context("to 3 chars with dots") { beforeEach { s = testString.padding(.left,toLength:3,withPad:".") }; it("will add 3 dots"  ) { expect(s).to(equal("..."     )) } }
                }
                context("right") {
                    context("to 8 chars"          ) { beforeEach { s = testString.padding(.right,toLength:8            ) }; it("will add 8 blanks") { expect(s).to(equal("        ")) } }
                    context("to 8 chars with dots") { beforeEach { s = testString.padding(.right,toLength:8,withPad:".") }; it("will add 8 dots"  ) { expect(s).to(equal("........")) } }
                    context("to 5 chars"          ) { beforeEach { s = testString.padding(.right,toLength:5            ) }; it("will add 5 blanks") { expect(s).to(equal("     "   )) } }
                    context("to 5 chars with dots") { beforeEach { s = testString.padding(.right,toLength:5,withPad:".") }; it("will add 5 dots"  ) { expect(s).to(equal("....."   )) } }
                    context("to 3 chars"          ) { beforeEach { s = testString.padding(.right,toLength:3            ) }; it("will add 3 blanks") { expect(s).to(equal("   "     )) } }
                    context("to 3 chars with dots") { beforeEach { s = testString.padding(.right,toLength:3,withPad:".") }; it("will add 3 dots"  ) { expect(s).to(equal("..."     )) } }
                }
                context("center") {
                    context("to 8 chars"          ) { beforeEach { s = testString.padding(.center,toLength:8            ) }; it("will prepend and append two blanks"  ) { expect(s).to(equal("        " )) } }
                    context("to 8 chars with dots") { beforeEach { s = testString.padding(.center,toLength:8,withPad:".") }; it("will prepend and append two dots"    ) { expect(s).to(equal("........" )) } }
                    context("to 9 chars"          ) { beforeEach { s = testString.padding(.center,toLength:9            ) }; it("will prepend and append three blanks") { expect(s).to(equal("         ")) } }
                    context("to 9 chars with dots") { beforeEach { s = testString.padding(.center,toLength:9,withPad:".") }; it("will prepend and append three dots"  ) { expect(s).to(equal(".........")) } }
                    context("to 5 chars"          ) { beforeEach { s = testString.padding(.center,toLength:5            ) }; it("will cut last 1 character"           ) { expect(s).to(equal("     "    )) } }
                    context("to 5 chars with dots") { beforeEach { s = testString.padding(.center,toLength:5,withPad:".") }; it("will cut last 1 character"           ) { expect(s).to(equal("....."    )) } }
                    context("to 3 chars"          ) { beforeEach { s = testString.padding(.center,toLength:3            ) }; it("will cut last 3 character"           ) { expect(s).to(equal("   "      )) } }
                    context("to 3 chars with dots") { beforeEach { s = testString.padding(.center,toLength:3,withPad:".") }; it("will cut last 3 character"           ) { expect(s).to(equal("..."      )) } }
                }
            }
            context("short string") {
                let testString = "1"
                var s = ""
                context("left") {
                    context("to 8 chars"          ) { beforeEach { s = testString.padding(      toLength:8            ) }; it("will append 7 blanks") { expect(s).to(equal("1       ")) } }
                    context("to 8 chars with dots") { beforeEach { s = testString.padding(      toLength:8,withPad:".") }; it("will append 7 dots"  ) { expect(s).to(equal("1.......")) } }
                    context("to 5 chars"          ) { beforeEach { s = testString.padding(      toLength:5            ) }; it("will append 4 blanks") { expect(s).to(equal("1    "   )) } }
                    context("to 5 chars with dots") { beforeEach { s = testString.padding(      toLength:5,withPad:".") }; it("will append 4 dots"  ) { expect(s).to(equal("1...."   )) } }
                    context("to 3 chars"          ) { beforeEach { s = testString.padding(.left,toLength:3            ) }; it("will append 2 blanks") { expect(s).to(equal("1  "     )) } }
                    context("to 3 chars with dots") { beforeEach { s = testString.padding(.left,toLength:3,withPad:".") }; it("will append 2 dots"  ) { expect(s).to(equal("1.."     )) } }
                }
                context("right") {
                    context("to 8 chars"          ) { beforeEach { s = testString.padding(.right,toLength:8            ) }; it("will prepend 7 blanks") { expect(s).to(equal("       1")) } }
                    context("to 8 chars with dots") { beforeEach { s = testString.padding(.right,toLength:8,withPad:".") }; it("will prepend 7 dots"  ) { expect(s).to(equal(".......1")) } }
                    context("to 5 chars"          ) { beforeEach { s = testString.padding(.right,toLength:5            ) }; it("will prepend 4 blanks") { expect(s).to(equal("    1"   )) } }
                    context("to 5 chars with dots") { beforeEach { s = testString.padding(.right,toLength:5,withPad:".") }; it("will prepend 4 dots"  ) { expect(s).to(equal("....1"   )) } }
                    context("to 3 chars"          ) { beforeEach { s = testString.padding(.right,toLength:3            ) }; it("will prepend 2 blanks") { expect(s).to(equal("  1"     )) } }
                    context("to 3 chars with dots") { beforeEach { s = testString.padding(.right,toLength:3,withPad:".") }; it("will prepend 2 dots"  ) { expect(s).to(equal("..1"     )) } }
                }
                context("center") {
                    context("to 8 chars"          ) { beforeEach { s = testString.padding(.center,toLength:8            ) }; it("will prepend and append 7 blanks") { expect(s).to(equal("   1    " )) } }
                    context("to 8 chars with dots") { beforeEach { s = testString.padding(.center,toLength:8,withPad:".") }; it("will prepend and append 7 dots"  ) { expect(s).to(equal("...1...." )) } }
                    context("to 9 chars"          ) { beforeEach { s = testString.padding(.center,toLength:9            ) }; it("will prepend and append 8 blanks") { expect(s).to(equal("    1    ")) } }
                    context("to 9 chars with dots") { beforeEach { s = testString.padding(.center,toLength:9,withPad:".") }; it("will prepend and append 8 dots"  ) { expect(s).to(equal("....1....")) } }
                    context("to 5 chars"          ) { beforeEach { s = testString.padding(.center,toLength:5            ) }; it("will prepend and append 4 blanks") { expect(s).to(equal("  1  "    )) } }
                    context("to 5 chars with dots") { beforeEach { s = testString.padding(.center,toLength:5,withPad:".") }; it("will prepend and append 4 dots"  ) { expect(s).to(equal("..1.."    )) } }
                    context("to 3 chars"          ) { beforeEach { s = testString.padding(.center,toLength:3            ) }; it("will prepend and append 2 blanks") { expect(s).to(equal(" 1 "      )) } }
                    context("to 3 chars with dots") { beforeEach { s = testString.padding(.center,toLength:3,withPad:".") }; it("will prepend and append 2 dots"  ) { expect(s).to(equal(".1."      )) } }
                }
            }
            context("long string") {
                let testString = "123456789012345678901234567890"
                var s = ""
                context("left") {
                    context("to 8 chars"          ) { beforeEach { s = testString.padding(      toLength:8            ) }; it("will cut to 8 chars") { expect(s).to(equal("12345678")) } }
                    context("to 8 chars with dots") { beforeEach { s = testString.padding(      toLength:8,withPad:".") }; it("will cut to 8 chars") { expect(s).to(equal("12345678")) } }
                    context("to 5 chars"          ) { beforeEach { s = testString.padding(      toLength:5            ) }; it("will cut to 5 chars") { expect(s).to(equal("12345"   )) } }
                    context("to 5 chars with dots") { beforeEach { s = testString.padding(      toLength:5,withPad:".") }; it("will cut to 5 chars") { expect(s).to(equal("12345"   )) } }
                    context("to 3 chars"          ) { beforeEach { s = testString.padding(.left,toLength:3            ) }; it("will cut to 3 chars") { expect(s).to(equal("123"     )) } }
                    context("to 3 chars with dots") { beforeEach { s = testString.padding(.left,toLength:3,withPad:".") }; it("will cut to 3 chars") { expect(s).to(equal("123"     )) } }
                }
                context("right") {
                    context("to 8 chars"          ) { beforeEach { s = testString.padding(.right,toLength:8            ) }; it("will cut to 8 chars") { expect(s).to(equal("12345678")) } }
                    context("to 8 chars with dots") { beforeEach { s = testString.padding(.right,toLength:8,withPad:".") }; it("will cut to 8 chars") { expect(s).to(equal("12345678")) } }
                    context("to 5 chars"          ) { beforeEach { s = testString.padding(.right,toLength:5            ) }; it("will cut to 5 chars") { expect(s).to(equal("12345"   )) } }
                    context("to 5 chars with dots") { beforeEach { s = testString.padding(.right,toLength:5,withPad:".") }; it("will cut to 5 chars") { expect(s).to(equal("12345"   )) } }
                    context("to 3 chars"          ) { beforeEach { s = testString.padding(.right,toLength:3            ) }; it("will cut to 3 chars") { expect(s).to(equal("123"     )) } }
                    context("to 3 chars with dots") { beforeEach { s = testString.padding(.right,toLength:3,withPad:".") }; it("will cut to 3 chars") { expect(s).to(equal("123"     )) } }
                }
                context("center") {
                    context("to 8 chars"          ) { beforeEach { s = testString.padding(.center,toLength:8            ) }; it("will cut to 8 chars") { expect(s).to(equal("12345678" )) } }
                    context("to 8 chars with dots") { beforeEach { s = testString.padding(.center,toLength:8,withPad:".") }; it("will cut to 8 chars") { expect(s).to(equal("12345678" )) } }
                    context("to 9 chars"          ) { beforeEach { s = testString.padding(.center,toLength:9            ) }; it("will cut to 9 chars") { expect(s).to(equal("123456789")) } }
                    context("to 9 chars with dots") { beforeEach { s = testString.padding(.center,toLength:9,withPad:".") }; it("will cut to 9 chars") { expect(s).to(equal("123456789")) } }
                    context("to 5 chars"          ) { beforeEach { s = testString.padding(.center,toLength:5            ) }; it("will cut to 5 chars") { expect(s).to(equal("12345"    )) } }
                    context("to 5 chars with dots") { beforeEach { s = testString.padding(.center,toLength:5,withPad:".") }; it("will cut to 5 chars") { expect(s).to(equal("12345"    )) } }
                    context("to 3 chars"          ) { beforeEach { s = testString.padding(.center,toLength:3            ) }; it("will cut to 3 chars") { expect(s).to(equal("123"      )) } }
                    context("to 3 chars with dots") { beforeEach { s = testString.padding(.center,toLength:3,withPad:".") }; it("will cut to 3 chars") { expect(s).to(equal("123"      )) } }
                }
            }
        }
    }
}
