//
//  RadioFeatureSpec.swift
//  Bert's RadiosTests
//
//  Created by vikingosegundo on 11/01/2022.
//

import Quick
import Nimble
import AudioStreaming
import BertModel
import BertsApp
@testable
import Bert_s_Radios

class RadioFeatureSpec:QuickSpec {
    override func spec() {
        var store      : AppStore!   ; beforeEach { store       = createDiskStore(pathInDocs:"RadioFeatureSpec.json")                         }; afterEach { destroy(&store)   }
        var audioPlayer: AudioPlayer!; beforeEach { audioPlayer = AudioPlayer()                                                               }; afterEach { audioPlayer = nil }
        var messages   : [Message]!  ; beforeEach { messages    = []                                                                          }; afterEach { messages    = []  }
        var feature    : Input!      ; beforeEach { feature     = createRadioFeature(store:store, player:audioPlayer) { messages.append($0) } }; afterEach { feature     = nil }
        describe("Radio Feature") {
            context("load all stations") {
                beforeEach {
                    feature(.radio(.load(.all(.stations))))
                }
                context("will result in 4 stations") {
                    it("in store"                                ) { expect(store.state().stations       ).to(haveCount(4)) }
                    it("loaded all stations message with payload") { expect(loadedStations(messages.last)).to(haveCount(4)) }
                    context("settings") {
                        beforeEach {
                            feature(.settings(.fetch(.all(.stations))))
                        }
                        it("fetches all stations to be used in settings") {
                            expect(store.state().settingsStations.map{$0.name}).to(equal(["Sublime","538","Veronica","Cosmo"])) } } } }
            context("play last station") {
                beforeEach{ feature(.radio(.load(.all(.stations)))) }
                beforeEach{ feature(.radio(.play(store.state().stations.last!)))}
                afterEach { feature(.radio(.pause)) }
                it("will be in store"            ) { expect(playingStationName(in:store)       ).toEventually(equal("Cosmo")) }
                it("will receive playing message") { expect(playingStation(messages.last)?.name).toEventually(equal("Cosmo")) }
                context("pause current station") {
                    beforeEach { feature(.radio(.pause)) }
                    context("will pause the station") {
                        it("in store"      ) { expect(currentlyPaused(in:store)         ).to(beTrue()    ) }
                        it("with a message") { expect(pausedStation(messages.last)?.name).to(equal("Cosmo")) } } }
                context("show info") {
                    it("received infos with a message") { expect(receivedSongInfo(messages.last)).toEventually(beTrue(), timeout:.seconds(600)) } } }
            context("vizualisation") {
                context("active") {
                    beforeEach {
                        feature(.radio(.load(.all(.stations))))
                        feature(.radio(.play(store.state().stations.first!))) }
                    afterEach { feature(.radio(.pause)) }
                    context("start"){
                        beforeEach { feature(.radio(.visualizer(.start))) }
                         afterEach { feature(.radio(.visualizer(.stop ))) }
                        it("results in loudness messages"){
                            expect(isLoudnessMessage(messages.reversed()[0])).toEventually(beTrue())
                            expect(isLoudnessMessage(messages.reversed()[1])).toEventually(beTrue()) } } }
                context("deactive") {
                    beforeEach {
                        feature(.radio(.visualizer(.deactivate))) }
                    context("start"){
                        beforeEach { feature(.radio(.visualizer(.start))) }
                         afterEach { feature(.radio(.visualizer(.stop ))) }
                        it("results in no loudness messages"){
                            expect(isLoudnessMessage(messages.reversed()[0])).toEventually(beFalse()) } } } }
            context("favourites"){
                context("add station to favourite") {
                    beforeEach { feature(.radio(.load(.all(.stations))))                              }
                    beforeEach { feature(.radio(.add(store.state().stations.first!, to: .favorites))) }
                    it("will be available in store") { expect(store.state().favoriteStations).to(equal([store.state().stations.first!])) }
                    context("and removing it") { beforeEach { feature(.radio(.remove(store.state().stations.first!, from: .favorites))) }
                        it("will no longer be in store") { expect(store.state().favoriteStations).to(equal([])) } } } }
            context("filter stations") {
                beforeEach { feature(.radio(.load(.all(.stations)))) }
                context("unfiltered"){
                    it("filtered stations are empty") { expect(store.state().filteredStations).to(beEmpty()) } }
                context("by name") {
                    context("starts with «5»") {
                        beforeEach { feature(.radio(.filter(by:.name("5")))) }
                        it("will expose results via filteredStations") {
                            expect(store.state().filteredStations.map{$0.name}).to(equal(["538"])) } }
                    context("contains «3»") {
                        beforeEach { feature(.radio(.filter(by:.name("3")))) }
                        it("will expose results via filteredStations") {
                            expect(store.state().filteredStations.map{$0.name}).to(equal(["538"])) } }
                    context("starts with «S»") {
                        beforeEach { feature(.radio(.filter(by:.name("S")))) }
                        it("will expose results via filteredStations") {
                            expect(store.state().filteredStations.map{$0.name}).to(equal(["Sublime","Cosmo"])) } }
                    context("starts with «s»") {
                        beforeEach { feature(.radio(.filter(by:.name("s")))) }
                        it("will expose results via filteredStations") {
                            expect(store.state().filteredStations.map{$0.name}).to(equal(["Sublime","Cosmo"])) } } } }
        }
    }
}
// helper functions to either extract object or evaluate via pattern matching to bool for simpler testing
fileprivate func currentlyPaused   (in store:AppStore) -> Bool      { if case     .paused                                           = store.state().currentStation { return true }; return false }
fileprivate func playingStationName(in store:AppStore) -> String?   { if case let .playing(songname:_,on:s)                         = store.state().currentStation { return s.name }; return nil }
fileprivate func pausedStation     (_    msg:Message?) -> Station?  { if case let .radio(.paused(s))                                = msg    { return s       }; return nil   }
fileprivate func playingStation    (_    msg:Message?) -> Station?  { if case let .radio(.playing(_,on:s))                          = msg    { return s       }; return nil   }
fileprivate func loadedStations    (_    msg:Message?) -> [Station] { if case let .radio(.loaded(.all(.stations(s),.successfully))) = msg    { return s       }; return []    }
fileprivate func isLoudnessMessage (_    msg:Message?) -> Bool      { if case     .radio(.visualizer(.loudness(interpolated:_)))    = msg    { return true    }; return false }
fileprivate func receivedSongInfo  (_    msg:Message?) -> Bool      { if case let .radio(.playing(s,on:_))                          = msg    { return s != "" }; return false }
