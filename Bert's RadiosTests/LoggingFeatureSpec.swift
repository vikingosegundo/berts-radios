//
//  LoggingFeatureSpec.swift
//  Bert's RadiosTests
//
//  Created by Manuel Meyer on 02.05.22.
//

import Quick
import Nimble
import BertModel
import BertsApp
@testable
import Bert_s_Radios

struct LoggerMock: Logging {
    let received: (LogItem) -> ()
    init(received r: @escaping (LogItem) -> ()) { received = r }
    func log(_ item: LogItem) { received(item) }
}
class LoggerSpec: QuickSpec {
    override func spec() {
        describe("Logger") {
            var logger: Logger!; beforeEach { logger = Logger() }; afterEach { logger = nil }
            context("Log") {
                context("info"  ) { beforeEach { logger.log(LogItem(text: "Hello World", logLevel: .info)         ) }; it("log() can be called") { expect(true).to(beTrue()) } }
                context("warn"  ) { beforeEach { logger.log(LogItem(text: "Hello World", logLevel: .warn)         ) }; it("log() can be called") { expect(true).to(beTrue()) } }
                context("error" ) { beforeEach { logger.log(LogItem(text: "Hello World", logLevel: .error)        ) }; it("log() can be called") { expect(true).to(beTrue()) } }
                context("custom") { beforeEach { logger.log(LogItem(text: "Hello World", logLevel: .custom("tag"))) }; it("log() can be called") { expect(true).to(beTrue()) } }
            }
        }
    }
}
class LoggingFeatureSpec: QuickSpec {
    override func spec() {
        var received: [LogItem] = []; beforeEach { received = [] }
        var feature : Input!        ; beforeEach { feature  = createLoggingFeature(logger: LoggerMock(received: {received.append($0)})) }; afterEach { feature     = nil }
        context("LoggingFeature") {
            context("info level") {
                beforeEach {
                    feature(.logging(.write(.init(text:"Hello", date:.today.noon, logLevel:.info))))
                }
                it("received one log item") {  expect(received).to(haveCount(1))                         }
                it("has text hello"       ) {  expect(received.first?.text).to(equal("Hello"))           }
                it("has date today noon"  ) {  expect(       received.first?.date).to(equal(.today.noon))}
                it("has info level"       ) {  expect(isInfo   (received.first!.logLevel)).to(beTrue())  }
                it("has no error level"   ) {  expect(isError  (received.first!.logLevel)).to(beFalse()) }
                it("has no warn level"    ) {  expect(isWarn   (received.first!.logLevel)).to(beFalse()) }
                it("has no custom tag"    ) {  expect(customTag(received.first!.logLevel)).to(beNil())   }
            }
            context("warn level") {
                beforeEach {
                    feature(.logging(.write(.init(text:"Hello", date:.today.noon, logLevel:.warn))))
                }
                it("received one log item") {  expect(received).to(haveCount(1))                         }
                it("has text hello"       ) {  expect(received.first?.text).to(equal("Hello"))           }
                it("has date today noon"  ) {  expect(received.first?.date).to(equal(.today.noon))       }
                it("has warn level"       ) {  expect(isWarn   (received.first!.logLevel)).to(beTrue())  }
                it("has no error level"   ) {  expect(isError  (received.first!.logLevel)).to(beFalse()) }
                it("has no info level"    ) {  expect(isInfo   (received.first!.logLevel)).to(beFalse()) }
                it("has no custom tag"    ) {  expect(customTag(received.first!.logLevel)).to(beNil())   }
            }
            context("error level") {
                beforeEach {
                    feature(.logging(.write(.init(text:"Hello", date:.today.noon, logLevel:.error))))
                }
                it("received one log item") {  expect(received).to(haveCount(1))                         }
                it("has text hello"       ) {  expect(received.first?.text).to(equal("Hello"))           }
                it("has date today noon"  ) {  expect(received.first?.date).to(equal(.today.noon))       }
                it("has no warn level"    ) {  expect(isWarn   (received.first!.logLevel)).to(beFalse()) }
                it("has error level"      ) {  expect(isError  (received.first!.logLevel)).to(beTrue() ) }
                it("has no info level"    ) {  expect(isInfo   (received.first!.logLevel)).to(beFalse()) }
                it("has no custom tag"    ) {  expect(customTag(received.first!.logLevel)).to(beNil()  ) }
            }
            context("custom tag level") {
                beforeEach {
                    feature(.logging(.write(.init(text:"Hello", date:.today.noon, logLevel:.custom("tag")))))
                }
                it("received one log item") {  expect(received).to(haveCount(1))                            }
                it("has text hello"       ) {  expect(received.first?.text).to(equal("Hello"))              }
                it("has date today noon"  ) {  expect(received.first?.date).to(equal(.today.noon))          }
                it("has no warn level"    ) {  expect(isWarn   (received.first!.logLevel)).to(beFalse())    }
                it("has no error level"   ) {  expect(isError  (received.first!.logLevel)).to(beFalse())    }
                it("has no info level"    ) {  expect(isInfo   (received.first!.logLevel)).to(beFalse())    }
                it("has a custom tag"     ) {  expect(customTag(received.first!.logLevel)).to(equal("tag")) }
            }
        }
    }
}
