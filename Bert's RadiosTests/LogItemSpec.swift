//
//  LogItemSpec.swift
//  Bert's RadiosTests
//
//  Created by Manuel Meyer on 02.05.22.
//

import Quick
import Nimble
import BertModel

func isInfo   (_ logLevel: LogItem.LogLevel) -> Bool    { if case           .info  = logLevel { return true }; return false }
func isError  (_ logLevel: LogItem.LogLevel) -> Bool    { if case           .error = logLevel { return true }; return false }
func isWarn   (_ logLevel: LogItem.LogLevel) -> Bool    { if case           .warn  = logLevel { return true }; return false }
func customTag(_ logLevel: LogItem.LogLevel) -> String? { if case let .custom(tag) = logLevel { return tag  }; return nil   }
class LogItemSpec: QuickSpec {
    override func spec() {
        context("LogItem") {
            context("LogLevel info") {
                let logItem = LogItem(text:"Hello World", date:.today.noon, logLevel:.info)
                it("has a text"   ) { expect(logItem.text).to(equal("Hello World") ) }
                it("has a date"   ) { expect(logItem.date).to(equal(.today.noon)   ) }
                it("is level info") { expect(isInfo(logItem.logLevel)).to(beTrue() ) }
            }
            context("LogLevel error") {
                let logItem = LogItem(text:"Hello World", date:.today.noon, logLevel:.error)
                it("has a text"    ) { expect(logItem.text).to(equal("Hello World")  ) }
                it("has a date"    ) { expect(logItem.date).to(equal(.today.noon)    ) }
                it("is level error") { expect(isError(logItem.logLevel)).to(beTrue() ) }
            }
            context("LogLevel warn") {
                let logItem = LogItem(text:"Hello World", date:.today.noon, logLevel:.warn)
                it("has a text"   ) { expect(logItem.text).to(equal("Hello World") ) }
                it("has a date"   ) { expect(logItem.date).to(equal(.today.noon)   ) }
                it("is level warn") { expect(isWarn(logItem.logLevel)).to(beTrue() ) }
            }
            context("LogLevel custom tag") {
                let logItem = LogItem(text:"Hello World", date:.today.noon, logLevel:.custom("tag"))
                it("has a text"    ) { expect(logItem.text).to(equal("Hello World")       ) }
                it("has a date"    ) { expect(logItem.date).to(equal(.today.noon)         ) }
                it("has custom tag") { expect(customTag(logItem.logLevel)).to(equal("tag")) }
            }
        }
    }
}
