//
//  miscellaneous.swift
//  BertTools
//
//  Created by vikingosegundo on 17/02/2022.
//

public func -<T: RangeReplaceableCollection>(lhs: T, rhs: T) -> T where T.Iterator.Element: Equatable {
    var lhs = lhs
    for element in rhs {
        if let index = lhs.firstIndex(of: element) { lhs.remove(at: index) }
    }
    return lhs
}

extension String {
    public enum Padding {
        public static var defaultPad: String.Element { " " }
        case left
        case right
        case center
    }
    public func padding(_ p:Padding = .left, toLength l:Int, withPad pad:String.Element = Padding.defaultPad) -> String {
        let s = String(prefix(l))
        func  append(to p:String, pad: String.Element) -> String { p + String(pad) }
        func prepend(to p:String, pad: String.Element) -> String { String(pad) + p }
        switch p {
        case .left  : return (0 ..< l - s.count).reduce(s) { p,_ in  append(to:p, pad:pad) }
        case .right : return (0 ..< l - s.count).reduce(s) { p,_ in prepend(to:p, pad:pad) }
        case .center: return (0 ..< l - s.count).reduce(s) { p,i in
            switch i % 2 {
            case 0 : return  append(to:p, pad:pad)
            default: return prepend(to:p, pad:pad)
            }
        }
        }
    }
}

import Foundation
public
extension Date {
    static var     today: Date { Date().midnight      }
    static var yesterday: Date { Date.today.dayBefore }
    static var  tomorrow: Date { Date.today.dayAfter  }
    
    var dayBefore: Date { cal.date(byAdding: .day, value: -1, to: self)! }
    var  dayAfter: Date { cal.date(byAdding: .day, value:  1, to: self)! }
    var  midnight: Date { cal.date(bySettingHour:  0, minute: 0, second: 0, of: self)!}
    var      noon: Date { cal.date(bySettingHour: 12, minute: 0, second: 0, of: self)!}
    
    private var cal: Calendar { Calendar.current }
}
